#include <iostream>
#include <fstream>
#include <complex>
#include <cstring>
#include "wat_primitives.hpp"
#include "impulse_response_plot.hpp"
#include "wat_math.hpp"

using namespace std;
using namespace sea_wat;

const size_t img_length = 36;
const size_t resp_length = 36;
typedef double data_type;
/*
data_type magn[img_length / 2 + 2] =
{
	0,
	0,
	0,
	-10.2 + 5,
	-11.8 + 5,
	-13.1 + 5,
	-14.2 + 5,
	-14.5 + 5,
	-14.8 + 5,
	-15. + 5,
	-24. + 5,
	-27. + 5,
	-27. + 5,
	-27. + 5,
	-27. + 5,
	-27. + 5,
	-27. + 5,
	-27. + 5,
	-27. + 5,
	-26.9 + 5,
	-26. + 5,
	-25. + 5,
	-24. + 5,
	-23. + 5,
	-22. + 5,
	-21. + 5,
	-20. + 5,
	-18.9 + 5,
	-17.9 + 5,
	-16.8 + 5,
	-16. + 5,
	-15.3 + 5,
	-14.5 + 5,
	-13.1 + 5,
	-13.8 + 5,
	-12.5 + 5,
	-12.4 + 5,
	-6.9 + 5,
	0,
	0,
};*/

data_type magn[img_length / 2 + 2] =
{
	0,
	0,
	-11.8 + 9,
	-14.2 + 9,
	-14.8 + 9,
	-24. + 9,
	-27. + 9,
	-27. + 9,
	-27. + 9,
	-27. + 9,
	-26. + 9,
	-24. + 9,
	-22. + 9,
	-20. + 9,
	-17.9 + 9,
	-16. + 9,
	-14.5 + 9,
	-13.8 + 9,
	-12.4 + 9,
	0,
};


void gain_adj(complex<double>* coeffs, float band, float gain_dB, size_t length)
{
	auto scaler = abs(dft(coeffs, length, band))
		/ max_datatype_value<float>() / dB_to_linear<float>(gain_dB);
	for (size_t coeff_index = 0; coeff_index < length; coeff_index++)
		coeffs[coeff_index] = complex<double>(static_cast<double>(coeffs[coeff_index].real() / scaler), 0);
}

int main()
{
	auto phase = new data_type[img_length];
	auto magnitude = new data_type[img_length];
	auto spectrum = new complex<data_type>[img_length];

	// convert to linear
	for (size_t sample_index = 0; sample_index < img_length / 2 + 2; sample_index++)
		magn[sample_index] = dB_to_linear<float>(magn[sample_index]);

	make_linear_phase(img_length, phase);
	memcpy(magnitude, magn, (img_length / 2 + 1) * sizeof(data_type));
	data_reverse(magnitude + 1, magnitude + img_length / 2, img_length - img_length / 2, 1); // allways mirrored

	for (size_t img_index = 0; img_index < img_length; img_index++)
	{
		spectrum[img_index] = magnitude[img_index] * exp(complex<data_type>(0, 1) * phase[img_index]);
	}
	delete[] phase;
	delete[] magnitude;

	auto response = new complex<double>[resp_length];
	for (size_t sample_index = 0; sample_index < resp_length; sample_index++)
	{
		auto sum = complex <double>(0, 0);
		for (size_t img_index = 0; img_index < img_length; img_index++)
			sum += spectrum[img_index] * exp(complex<data_type>(0, 1) * pi_x2 * static_cast<double>(sample_index * img_index) / static_cast<double>(img_length));
		response[sample_index] = sum;
	}
	gain_adj(response, 0, 0, resp_length);

	auto magnitude_int32 = new int32_t[img_length];
	for (size_t sample_index = 0; sample_index < img_length; sample_index++)
	{
		auto in = response[sample_index].real();
		to_fixed32(&in, &magnitude_int32[sample_index], 1, 1);
	}
	py_script_build<int32_t>(magnitude_int32, resp_length, "../samples/fir_test.py");
	delete[] magnitude_int32;
	delete[] spectrum;
}
#include <iostream>
#include <cstring>
#include "wat_fir_Wiener_Hopf.hpp"
#include "wat_wave.hpp"
#include "impulse_response_plot.hpp"

using namespace std;
using namespace sea_wat;

int main()
{
	typedef float data_type;
	const size_t buffer_size = 100;
	wave_reader_t src_ref;
	wave_reader_t src;
	wave_writer_t dst;
	// open function requires filename and maximum buffer size
	auto err_code = src_ref.open(wave_read_set_t
	{
		buffer_size,
		//"../samples/white_48000.wav"
		//"../mus_tst/Nelly_Furtado-Say_It_Right.wav"
		//"../mus_tst/mus_tst.wav"
		//"../mus_tst/Voodoo_People.wav"
		//"../mus_tst/Voodoo_People_reverb.wav"
		"../mus_tst/ref.wav"
	});
	if (err_code)
	{
		cerr << err_code.message() << endl;
		return -1;
	}
	err_code = src.open(wave_read_set_t
	{
		buffer_size,
		//"../mus_tst/Nelly_Furtado-Say_It_Right_noised.wav"
		//"../mus_tst/mus_tst_noised.wav"
		//"../mus_tst/Voodoo_People_noised.wav"
		//"../mus_tst/Voodoo_People.wav"
		//"../mus_tst/Voodoo_People_reverb.wav"
		"../mus_tst/mic3.wav"
	});
	if (err_code)
	{
		cerr << err_code.message() << endl;
		return -1;
	}

	dst.open(wave_write_set_t
	{
		src_ref.get_format(),
		src_ref.get_samplerate(),
		src_ref.get_n_channels(),
		src_ref.get_bits_per_sample(),
		buffer_size,
		"../samples/adaptive_err.wav"
	});
	auto fir_adaptive = make_unique<fir_Wiener_Hopf_t<data_type>>();
	auto in_ref_workbuffer = new data_type[buffer_size * src_ref.get_n_channels()];
	auto in_workbuffer = new data_type[buffer_size * src_ref.get_n_channels()];
	auto out_workbuffer = new data_type[buffer_size * src_ref.get_n_channels()];
	auto samples_cnt = src.get_n_samples();
	int32_t counter = 0;
	while (samples_cnt)
	{
		auto n_samples = buffer_size > samples_cnt ? samples_cnt : buffer_size;
		src_ref.read(in_ref_workbuffer, n_samples);
		src.read(in_workbuffer, n_samples);
		fir_adaptive->process(in_workbuffer, in_ref_workbuffer, out_workbuffer, n_samples);
		dst.write(out_workbuffer, n_samples);
		samples_cnt -= n_samples;
	}
	delete[] in_ref_workbuffer;
	delete[] in_workbuffer;
	delete[] out_workbuffer;

	py_script_build(static_cast<data_type*>(fir_adaptive->get_model(0)), fir_adaptive->get_length(), "../samples/fir_test.py");
}
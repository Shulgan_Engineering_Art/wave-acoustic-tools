#include <iostream>
#include "wat_wave.hpp"
#include "wat_fir_resampler.hpp"
using namespace std;
using namespace sea_wat;

int main(void)
{
	typedef double resempler_data_type;
	const int32_t dst_samplerate = 48000;
	const size_t buffer_size = 40;
	wave_reader_t src;
	wave_writer_t dst;
	// "open" function requires filename and maximum buffer size
	auto err_code = src.open(wave_read_set_t
	{ 
		buffer_size,
		"../samples/sine_stereo_44100_pcm.wav" 
	});
	if (err_code)
	{
		cerr << err_code.message() << endl;
		return -1;
	}
	auto resampler = fir_resampler_t<resempler_data_type>(fir_resampler_set_Hz_t
	{ 
		src.get_samplerate(), 
		dst_samplerate, 
		src.get_n_channels(),
		-60.f 
	});
	auto in = new resempler_data_type[buffer_size * src.get_n_channels()];
	// out_size = in_size * rate + 1
	auto out_buffer_size = buffer_size * resampler.int_scaler / resampler.dec_scaler + 1;
	auto out = new resempler_data_type[out_buffer_size * src.get_n_channels()];
	dst.open(wave_write_set_t
	{
		src.get_format(),
		dst_samplerate,
		src.get_n_channels(),
		src.get_bits_per_sample(),
		buffer_size * resampler.int_scaler / resampler.dec_scaler + 1,
		"../samples/sine_stereo_48000.wav"
	});
	auto samples_cnt = src.get_n_samples();
	while (samples_cnt)
	{
		auto in_n_samples = buffer_size > samples_cnt ? samples_cnt : buffer_size;
		src.read(in, in_n_samples);
		auto out_n_samples = resampler.process(in, out, in_n_samples);
		dst.write(out, out_n_samples);
		samples_cnt -= in_n_samples;
	}
	samples_cnt = resampler.get_flush_size();
	while (samples_cnt)
	{
		// CAUTION. Use "out buffer size" instead "in buffer size".
		auto out_n_samples = out_buffer_size > samples_cnt ? samples_cnt : out_buffer_size;
		out_n_samples = resampler.flush(out, out_n_samples);
		out_n_samples = out_n_samples < samples_cnt ? out_n_samples : samples_cnt;
		dst.write(out, out_n_samples);
		samples_cnt -= out_n_samples;
	}
	delete[] out;
    delete[] in;
}

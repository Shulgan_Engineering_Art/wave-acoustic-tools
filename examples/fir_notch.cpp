#include <iostream>
#include "wat_wave.hpp"
#include "wat_fir.hpp"
#include "impulse_response_plot.hpp"

using namespace std;
using namespace sea_wat;

int main(void)
{
	typedef float fir_data_type;
	const size_t buffer_size = 22;
	wave_reader_t src;
	wave_writer_t dst;
	// open function requires filename and maximum buffer size
	auto err_code = src.open(wave_read_set_t
	{ 
		buffer_size,
		"../samples/white_48000.wav"
	});
	if (err_code)
	{
		cerr << err_code.message() << endl;
		return -1;
	}
	dst.open(wave_write_set_t
	{
		src.get_format(),
		src.get_samplerate(),
		src.get_n_channels(),
		src.get_bits_per_sample(),
		buffer_size,
		"../samples/band_stop_48000.wav"
	});
	auto fs = src.get_samplerate();
	auto in_workbuffer = new fir_data_type[buffer_size * src.get_n_channels()];
	auto out_workbuffer = new fir_data_type[buffer_size * src.get_n_channels()];
	auto filter = fir_t<fir_data_type>(fir_set_Hz_t
	{ 
		band_stop, 
		Kaiser_win, 
		src.get_n_channels(), 
		4500, 
		5500, 
		static_cast<uint32_t>(fs * 0.011f), 
		-70.f, 
		0.f,
		-70.f,
		fs 
	});

	auto samples_cnt = src.get_n_samples();
	while (samples_cnt)
	{
		auto n_samples = buffer_size > samples_cnt ? samples_cnt : buffer_size;
		src.read(in_workbuffer, n_samples);
		filter.process(in_workbuffer, out_workbuffer, n_samples);
		dst.write(out_workbuffer, n_samples);
		samples_cnt -= n_samples;
	}
	delete[] in_workbuffer;
	delete[] out_workbuffer;

	py_script_build<fir_data_type>(static_cast<fir_data_type*>(filter.get_model()), filter.get_length(), "../samples/fir_test.py");
}

#include <iostream>
#include "wat_wave.hpp"
#include "wat_spectrum_dft.hpp"
using namespace std;
using namespace sea_wat;
/*
int main(void)
{
	typedef float spectrum_an_data_type;
	const size_t buffer_size = 128;
	const uint32_t spectrum_n_bands = 1000;
	const auto spectrum_begin = 0.01f;
	const auto spectrum_end = 0.1f;
	auto src = make_unique<wave_reader_t>();
	auto err_code = src->open(wave_read_set_t 
	{ 
		buffer_size, 
		"../samples/sine1k_44100.wav"
	});
	if (err_code)
	{
		cerr << err_code.message() << endl;
		return -1;
	}
	fstream specrum_log("../samples/spectrum.txt", ios::out);
    auto workbuff = new spectrum_an_data_type[buffer_size * src->get_n_channels()];
	spectrum_dft_t<spectrum_an_data_type> analyzer = spectrum_dft_set_t
	{
		src->get_n_channels(),
		spectrum_n_bands,
		spectrum_begin,
		spectrum_end,
		0,						// calculating entire signal, overlaps no needed
		src->get_n_samples(),	// window size = signal size
		1
	};
	auto samples_cnt = src->get_n_samples();
	while (samples_cnt)
	{
		auto in_n_samples = buffer_size > samples_cnt ? samples_cnt : buffer_size;
		src->read(workbuff, in_n_samples);
		analyzer.process(workbuff, in_n_samples);
		samples_cnt -= in_n_samples;
	}
	vector<vector<float*>> amplitudes;
	// Spectrum will be ready after the window will end.
	analyzer.get_amplitudes(&amplitudes);
	auto freq_step = static_cast<float>(src->get_samplerate()) * (spectrum_end - spectrum_begin) / spectrum_n_bands;
	float freq = static_cast<float>(src->get_samplerate()) * spectrum_begin; 
	for (auto amplitude = amplitudes.back().begin(); amplitude < amplitudes.back().end(); amplitude++)
	{
		specrum_log << "freq, Hz  ----- channel x" << endl;
		auto band_value = *amplitude;
		for (size_t band_index = 0; band_index < spectrum_n_bands; band_index++)
		{
			specrum_log << freq << "	" << *band_value++ << endl;
			freq += freq_step;
		}
	}
	delete[] workbuff;
	
	// Free allocated in the wat_spectrum_dft buffers
	for (auto amplitude = amplitudes.back().begin(); amplitude < amplitudes.back().end(); amplitude++)
		delete[] *amplitude;
}
*/


// waterfall
int main(void)
{
	typedef float spectrum_an_data_type;
	const size_t buffer_size = 128;
	const uint32_t spectrum_n_bands = 1000;
	const auto spectrum_begin = 0.01f;
	const auto spectrum_end = 0.1f;
	auto src = make_unique<wave_reader_t>();
	auto err_code = src->open(wave_read_set_t
	{
		buffer_size,
		"../samples/chirp_16k.wav"
	});
	if (err_code)
	{
		cerr << err_code.message() << endl;
		return -1;
	}
	fstream specrum_log("../samples/spectrum.txt", ios::out);
	auto workbuff = new spectrum_an_data_type[buffer_size * src->get_n_channels()];
	spectrum_dft_t<spectrum_an_data_type> analyzer = spectrum_dft_set_t
	{
		src->get_n_channels(),
		spectrum_n_bands,
		spectrum_begin,
		spectrum_end,
		2200,						
		4000,	
		1
	};
	auto samples_cnt = src->get_n_samples();
	while (samples_cnt)
	{
		auto in_n_samples = buffer_size > samples_cnt ? samples_cnt : buffer_size;
		src->read(workbuff, in_n_samples);
		analyzer.process(workbuff, in_n_samples);
		samples_cnt -= in_n_samples;
	}
	vector<vector<float*>> amplitudes;
	// Spectrum will be ready after the window will end.
	analyzer.get_amplitudes(&amplitudes);
	auto freq_step = static_cast<float>(src->get_samplerate()) * (spectrum_end - spectrum_begin) / spectrum_n_bands;
	
	for (auto window : amplitudes)
	{
		float freq = static_cast<float>(src->get_samplerate()) * spectrum_begin;
		for (auto channel : window)
		{
			specrum_log << "freq, Hz  ----- channel x" << endl;
			auto band_value = channel;
			for (size_t band_index = 0; band_index < spectrum_n_bands; band_index++)
			{
				specrum_log << freq << "	" << *band_value++ << endl;
				freq += freq_step;
			}
		}
	}
	delete[] workbuff;

	// Free allocated in the wat_spectrum_dft buffers
	for (auto amplitude = amplitudes.back().begin(); amplitude < amplitudes.back().end(); amplitude++)
		delete[] * amplitude;
}

#include <iostream>
#include <memory>
#include "wat_primitives.hpp"
#include "wat_wave.hpp"
using namespace std;
using namespace sea_wat;

int main(void)
{
	const size_t buffer_size = 64;
	auto src = make_unique<wave_reader_t>();
	auto dst = make_unique<wave_writer_t>();
	// open function requires filename and maximum buffer size
	auto err_code = src->open(wave_read_set_t 
	{ 
		buffer_size,
		"../samples/sine_stereo_44100_float.wav"
	});
	if (err_code)
	{
		cerr << err_code.message() << endl;
		return -1;
	}
	dst->open(wave_write_set_t 
	{ 
		WAVE_FORMAT_PCM, 
		src->get_samplerate(), 
		src->get_n_channels(), 
		static_cast<uint16_t>(sizeof(int16_t) * 8), 
		buffer_size, 
		"../samples/fixed16_44100.wav"
	});

    // allocate 24 bit buffer, uses int32_t
    auto workbuff_24bit = new int32_t[buffer_size * src->get_n_channels()];
    // allocate deinterleaved 8 bit buffer
    auto workbuff_8bit = new int8_t*[src->get_n_channels()];
    for (size_t channel_index = 0; channel_index < src->get_n_channels(); channel_index++)
        workbuff_8bit[channel_index] = new int8_t[buffer_size];
    auto samples_cnt = src->get_n_samples();
	while (samples_cnt)
	{
		auto n_samples = buffer_size > samples_cnt ? samples_cnt : buffer_size;
        // read as 24 bit
        // 24 bit depth requires manually setting the size
        src->read(workbuff_24bit, n_samples, sizeof_fixed24);
        // how to deinterleave:
        // convert to 8 bit deinterleaved (signed)
        to_fixed8(workbuff_24bit, workbuff_8bit, n_samples, src->get_n_channels(), sizeof_fixed24);
        // save as 16 bit
        dst->write(workbuff_8bit, n_samples);
		samples_cnt -= n_samples;
	}
	delete[] workbuff_24bit;
    for (size_t channel_index = 0; channel_index < src->get_n_channels(); channel_index++)
    	delete[] workbuff_8bit[channel_index];
    delete[] workbuff_8bit;
}

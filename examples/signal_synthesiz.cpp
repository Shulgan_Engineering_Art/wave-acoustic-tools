#include <iostream>
#include "wat_wave.hpp"
#include "wat_sine_synthesizer.hpp"
#include "wat_math.hpp"
using namespace std;
using namespace sea_wat;

int main(void)
{
	// synthesis the rectangular signal from n_harmonics of first harmonics
	typedef int16_t synthesizer_data_type;
	const size_t buffer_size = 64;
    const uint32_t dst_samplerate = 8000;
	const uint32_t dst_freqency = 30;
	const size_t n_channels = 1;
	const uint32_t n_harmonics = 5;
    const size_t duration_sec = 2;
	wave_writer_t dst;
	dst.open(wave_write_set_t 
	{ 
		WAVE_FORMAT_PCM,
		dst_samplerate,
		static_cast<uint32_t>(n_channels), 
		static_cast<uint16_t>(sizeof(synthesizer_data_type) * 8),
		buffer_size, 
		"../samples/synthesiz_test.wav"
	});
    /*vector<unique_ptr<sine_synthesizer_t<synthesizer_data_type>>> synthesizers;
	for (uint32_t hatmonic_index = 0; hatmonic_index < n_harmonics; hatmonic_index++)
	{
		synthesizers.emplace_back(make_unique<sine_synthesizer_t<synthesizer_data_type>>(
			sine_synthesizer_set_Hz_t
		{
			dst_samplerate,
			dst_freqency * (hatmonic_index * 2 + 1),
			n_channels,
			linear_to_dB(0.8f / (hatmonic_index * 2 + 1))
		}));
	}
    auto workbuff = new synthesizer_data_type[buffer_size];
    auto samples_cnt = dst_samplerate * duration_sec;
	while (samples_cnt)
	{
		auto n_samples = buffer_size > samples_cnt ? samples_cnt : buffer_size;
        auto synthesizer = synthesizers.begin();
		// write signal to workbuff
		(*synthesizer)->process(workbuff, n_samples);
		for (synthesizer++; synthesizer < synthesizers.end(); synthesizer++)
			// add signal to workbuff
			(*synthesizer)->process(workbuff, n_samples, true);
		dst->write(workbuff, n_samples);
		samples_cnt -= n_samples;
	}*/





	// TODO: has difference with audacity
	auto synthesizer = sine_synthesizer_t<synthesizer_data_type>(sine_synthesizer_set_Hz_t
	{
		dst_samplerate,
		1,
		1,
		-10.f
	});
	auto workbuff = new synthesizer_data_type[buffer_size];
	const size_t n_samples_one_freq = 400; // 0.05 sec
	const size_t n_bands = 4000;
	float freq_step = 8000 / 2 / n_bands;
	
	for (size_t band_index = 0; band_index < n_bands; band_index++)
	{
		synthesizer.set_freqency(freq_step * band_index + 1);
		for (size_t sample_index = 0; sample_index < n_samples_one_freq; sample_index += buffer_size)
		{
			synthesizer.process(workbuff, buffer_size);
			dst.write(workbuff, buffer_size);
		}
		synthesizer.process(workbuff, n_samples_one_freq % buffer_size);
		dst.write(workbuff, n_samples_one_freq % buffer_size);
	}
	delete[] workbuff;
}

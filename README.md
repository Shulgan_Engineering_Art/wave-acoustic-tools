# wave-acoustic-tools
C++ library. Provides by Shulgan Engineering Art. The library is for signal processing research and application development. Library provides algorithms for digital sound, signal processing, signal analysis.
## Features
- 8, 16, 24, 32 bit depth fixed point and 32 and 64 bit depth float point data processing.
- Cross platform. Little and big endian platforms.
- Supports .wav format only.
- Signal conversion: bit depth, samplerate conversion, interleaving - deinterleaving.
- Spectrum analysis.
- Wavelet analysis. (planned)
- POLQA, PESQ analysis. (planned)
- Echo cancellation. (in progress)
- Amplitude analysis: peak, RMS, Replaygain*. (planned)
- True bit depth analysis. (planned)
- Dynamic range compressing. (planned)
- Equalizing. (planned)
- Echo*.
- Reverberation*.

(*) - may be moved to another music lib.
## Build Requirements
- C++17 or later
- cmake 3.10 or later
## Notes

## Prepare to building
```sh
git clone git@gitlab.com:Shulgan_Engineering_Art/wave-acoustic-tools.git
cd wave-acoustic-tools
mkdir build
cd build
cmake ..
```
## License
wave-acoustic-tools is covered by a Commercial/[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html) license.
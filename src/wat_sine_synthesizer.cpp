/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#include <cassert>
#include <type_traits>
#if defined(WAT_OPENMP_ENABLE)
#include <omp.h>
#endif
#include "wat_primitives.hpp"
#include "wat_math.hpp"
#include "wat_sine_synthesizer.hpp"

namespace sea_wat
{

template class sine_synthesizer_t<float>;
template class sine_synthesizer_t<double>;
template class sine_synthesizer_t<int8_t>;
template class sine_synthesizer_t<int16_t>;
template class sine_synthesizer_t<int32_t>;

template <typename data_t>
sine_synthesizer_t<data_t>::sine_synthesizer_t(const sine_synthesizer_set_Hz_t& set, size_t out_datasize)
{
    this->init(set);
    this->out_datasize = out_datasize;
}

template <typename data_t>
sine_synthesizer_t<data_t>::sine_synthesizer_t(size_t out_datasize)
{
    this->init(sine_synthesizer_default);
    this->out_datasize = out_datasize;
}

template <typename data_t>
sine_synthesizer_t<data_t>::~sine_synthesizer_t()
{
    this->dealloc();
}

template <typename data_t>
void sine_synthesizer_t<data_t>::init(const sine_synthesizer_set_Hz_t& set)
{
    assert(set.freqency_Hz < set.samplerate_Hz);
    assert(set.gain_dB <= 0.f);
    this->dealloc();
    this->samplerate_Hz = set.samplerate_Hz;
    this->gain_scaler = dBFS_to_linear<data_t>(set.gain_dB, this->out_datasize);
    this->gain_dB = set.gain_dB;
    this->n_channels = set.n_channels;
    this->phase = static_cast<int64_t>(set.start_phase * phase_scaler);
    this->set_freqency(set.freqency_Hz);
}

template <typename data_t>
void sine_synthesizer_t<data_t>::set_freqency(uint32_t freqency_Hz)
{
    //assert(switching_time_uS >= 0);
    this->phase_step = static_cast<int64_t>(pi_x2 * freqency_Hz * 
        phase_scaler / this->samplerate_Hz); 
}

template <typename data_t>
void sine_synthesizer_t<data_t>::set_gain_dB(float gain_dB)
{
    // planned
}

template <typename data_t>
void sine_synthesizer_t<data_t>::set_gain(float gain)
{

}

template <typename data_t>
size_t sine_synthesizer_t<data_t>::process(data_t* out, size_t n_samples, bool out_add)
{
    vector<data_t*> out_vec;
    for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
		out_vec.push_back(out + channel_index);
    return this->process(out_vec, n_samples, this->n_channels, 
		out_add ? make_out_add<data_t> : make_out_move<data_t>);
}

template <typename data_t>
size_t sine_synthesizer_t<data_t>::process(data_t** out, size_t n_samples, bool out_add)
{
    vector<data_t*> out_vec;
    for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
		out_vec.push_back(out[channel_index]);
    return this->process(out_vec, n_samples, 1, 
		out_add ? make_out_add<data_t> : make_out_move<data_t>);
}

template <typename data_t>
size_t sine_synthesizer_t<data_t>::process(const vector<data_t*>& out, size_t n_samples, 
    ptrdiff_t out_step, function<make_out_t<data_t>>& make_out)
{
    assert(out.size() == this->n_channels);
	auto out_vec = out;
#if defined(WAT_OPENMP_ENABLE)
#pragma omp parallel for
#endif
    for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
    {
        for (auto out_for_channel = out_vec.begin(); out_for_channel < out_vec.end(); out_for_channel++)
        {
			// possible more optimizations, if use sin(fixed point value with range -1..1)
            make_out(*(*out_for_channel), static_cast<data_t>(sinf(static_cast<float>(this->phase) /
                phase_scaler) * this->gain_scaler));
			(*out_for_channel) += out_step;
        }
        this->phase += this->phase_step;
        this->phase = this->phase % pi_x2_scaled;
    }
    return n_samples;
}

template <typename data_t>
float sine_synthesizer_t<data_t>::get_phase()
{
    return static_cast<float>(this->phase / phase_scaler);
}

template <typename data_t>
void sine_synthesizer_t<data_t>::dealloc()
{

}

const sine_synthesizer_set_Hz_t sine_synthesizer_default = {44100, 5000, 1, -3, 0};
}
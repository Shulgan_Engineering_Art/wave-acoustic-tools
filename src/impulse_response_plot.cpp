#include <iostream>
#include <fstream>
#include "impulse_response_plot.hpp"

namespace sea_wat
{
const string first_part = "import numpy as np \n"
"import matplotlib.pyplot as plt \n"
"\n"
"# Size of FFT analysis \n"
"N = 1024 \n"
"\n"
"def fir_freqz(b) : \n"
"	# Get the frequency response \n"
"	X = np.fft.fft(b, N) \n"
"	# Take the magnitude \n"
"	Xm = np.abs(X) \n"
"	# Convert the amplitude to decibel scale \n"
"	Xdb = 20 * np.log10(Xm / Xm.max()) \n"
"	# Frequency vector \n"
"	f = np.arange(N) / N \n"
"\n"
"	return Xdb, f \n"
"\n"
"if __name__ == \"__main__\": \n"
"\n"
"#FIR filter coefficients \n"
"   b = np.array([\n";

const string second_part = "]) \n"
"\n"
"\n"
"# Get frequency response of filter \n"
"Xdb, f = fir_freqz(b) \n"
"# ... and it mirrored version \n"

"# Plot the impulse response \n"
"plt.subplot(211) \n"
"plt.stem(b, linefmt = 'b-', markerfmt = 'bo', basefmt = 'k-') \n"
"plt.grid(True) \n"
"\n"
"plt.title('Impulse reponse') \n"
"plt.xlabel('Sample') \n"
"plt.ylabel('Amplitude') \n"
"plt.legend() \n"
"\n"
"# Plot the frequency response \n"
"plt.subplot(212) \n"
"plt.plot(f, Xdb, 'b') \n"
"plt.grid(True) \n"
"\n"
"plt.title('Frequency reponse') \n"
"plt.xlabel('Frequency [Hz]') \n"
"plt.ylabel('Amplitude [dB]') \n"
"plt.xlim((0, 0.5)) # Set the frequency limit - being lazy \n"
"plt.legend() \n"
"\n"
"plt.show()";

template void py_script_build<>(float*,   size_t, string);
template void py_script_build<>(double*,  size_t, string);
template void py_script_build<>(int8_t*,  size_t, string);
template void py_script_build<>(int16_t*, size_t, string);
template void py_script_build<>(int32_t*, size_t, string);
template <typename data_t>
void py_script_build(data_t* coeff, size_t length, string filename)
{
    auto coeff_last = coeff + length - 1;
    ofstream coeffs;
	coeffs.open(filename);
	coeffs << first_part;
	// save coeffs
	for (; coeff <= coeff_last; coeff++)
		coeffs << *coeff << "," << endl;
	coeffs << second_part;
	coeffs.close();	
}

template void py_script_build<>(complex<float>*, size_t, string);
template void py_script_build<>(complex<double>*, size_t, string);
template void py_script_build<>(complex<int8_t>*, size_t, string);
template void py_script_build<>(complex<int16_t>*, size_t, string);
template void py_script_build<>(complex<int32_t>*, size_t, string);
template <typename data_t>
void py_script_build(complex<data_t>* coeff, size_t length, string filename)
{
	auto coeff_last = coeff + length - 1;
	ofstream coeffs;
	coeffs.open(filename);
	coeffs << first_part;
	// save coeffs
	for (; coeff <= coeff_last; coeff++)
		coeffs << coeff->real() << "," << endl;
	coeffs << second_part;
	coeffs.close();
}
}
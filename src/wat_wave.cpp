/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#include <type_traits>
#include "wat_primitives.hpp"
#include "wat_wave.hpp"

namespace sea_wat
{

wave_t::wave_t()
{
	this->fmt = default_fmt;
	this->data = default_data;
	this->n_samples_buffer_size = 0;
	this->n_samples_remaining = 0;
	this->current_sample_index = 0;
	this->open_stream_last_error = error_code{};
}

wave_t::~wave_t()
{
	this->dealloc();
}

void wave_t::dealloc()
{
	if (this->transit_buffer.has_value())
	{
		visit([](auto&& buff_ptr){ delete[] buff_ptr; }, this->transit_buffer.value());
		this->transit_buffer.reset();
	}
}

error_code wave_t::allocate_transition_buffer()
{
	auto total_samples = this->n_samples_buffer_size * this->fmt.n_channels;
	if (this->fmt.format == WAVE_FORMAT_PCM)
	{
		if (this->fmt.bits_per_sample == sizeof(int8_t) * 8)
			this->transit_buffer = new int8_t[total_samples];
		else if (this->fmt.bits_per_sample == sizeof(int16_t) * 8)
			this->transit_buffer = new int16_t[total_samples];
		else if ( (this->fmt.bits_per_sample == sizeof_fixed24 * 8) ||
			(this->fmt.bits_per_sample == sizeof(int32_t) * 8) )
			this->transit_buffer = new int32_t[total_samples];
		else
			this->open_stream_last_error = make_error_code(errc::not_supported);
	}
	else if (this->fmt.format == WAVE_FORMAT_IEEE_FLOAT)
	{
		if (this->fmt.bits_per_sample == sizeof(float) * 8)
			this->transit_buffer = new float[total_samples];
		else if (this->fmt.bits_per_sample == sizeof(double) * 8)
			this->transit_buffer = new double[total_samples];
		else
			this->open_stream_last_error = make_error_code(errc::not_supported);
	}
	else
		this->open_stream_last_error = make_error_code(errc::not_supported);
	return this->open_stream_last_error;
}

#if defined(WAT_IS_BIG_ENDIAN_SYS)
inline void wave_t::fmt_byteswap(subchunk_fmt_header_t* fmt)
{
	fmt->head.chunk_size = _byteswap_ulong(fmt->head.chunk_size);
	fmt->format = _byteswap_ushort(fmt->format);
	fmt->n_channels = _byteswap_ushort(fmt->n_channels);
	fmt->sample_rate = _byteswap_ushort(fmt->sample_rate);
	fmt->byte_rate = _byteswap_ushort(fmt->byte_rate);
	fmt->block_align = _byteswap_ushort(fmt->block_align);
	fmt->bits_per_sample = _byteswap_ushort(fmt->bits_per_sample);
}
#endif

size_t wave_t::get_samples_remaining()
{
	return this->n_samples_remaining;
}

uint16_t wave_t::get_n_channels() const
{
	return this->fmt.n_channels;
}

uint32_t wave_t::get_samplerate() const
{
	return this->fmt.sample_rate;
}

uint16_t wave_t::get_bits_per_sample() const
{
	return this->fmt.bits_per_sample;
}

size_t wave_t::get_n_samples()
{
	return data.head.chunk_size / fmt.bits_per_sample * 8 / this->fmt.n_channels;
}

size_t wave_t::get_sample_index()
{
	return this->current_sample_index;
}

inline error_code wave_t::state_check(size_t n_samples, size_t* file_bytes_per_sample)
{
	if (this->open_stream_last_error)
		return this->open_stream_last_error;
	if (n_samples > this->n_samples_buffer_size || !this->transit_buffer.has_value())
		return make_error_code(errc::no_buffer_space);
	if (n_samples > this->n_samples_remaining)
		return make_error_code(errc::value_too_large);

	this->n_samples_remaining -= n_samples;
	this->current_sample_index += n_samples;
	*file_bytes_per_sample = this->fmt.bits_per_sample / 8;
	return error_code{};
}

template <typename in_data_t, typename out_data_t>
inline void converter(in_data_t* in, out_data_t* out, size_t n_samples, size_t n_channels, 
	size_t in_datasize, [[maybe_unused]] size_t out_datasize)
{
	if constexpr (is_same_v<out_data_t, int8_t>)
		to_fixed8(in, out, n_samples, n_channels, in_datasize);		
	else if constexpr (is_same_v<out_data_t, int16_t>)
		to_fixed16(in, out, n_samples, n_channels, in_datasize);
	else if constexpr (is_same_v<out_data_t, int32_t>)
    {
		if (out_datasize == sizeof_fixed24)
			to_fixed24(in, out, n_samples, n_channels, in_datasize);
		else
			to_fixed32(in, out, n_samples, n_channels, in_datasize);
    }
	else if constexpr (is_same_v<out_data_t, float>)
		to_float32(in, out, n_samples, n_channels, in_datasize);
	else if constexpr (is_same_v<out_data_t, double>)
		to_float64(in, out, n_samples, n_channels, in_datasize);
}

template <typename in_data_t, typename out_data_t>
inline void converter(in_data_t** in, out_data_t* out, size_t n_samples, size_t n_channels, 
	size_t in_datasize, [[maybe_unused]] size_t out_datasize)
{
	if constexpr (is_same_v<out_data_t, int8_t>)
		to_fixed8(in, out, n_samples, n_channels, in_datasize);		
	else if constexpr (is_same_v<out_data_t, int16_t>)
		to_fixed16(in, out, n_samples, n_channels, in_datasize);
	else if constexpr (is_same_v<out_data_t, int32_t>)
    {
		if (out_datasize == sizeof_fixed24)
			to_fixed24(in, out, n_samples, n_channels, in_datasize);
		else
			to_fixed32(in, out, n_samples, n_channels, in_datasize);
    }
	else if constexpr (is_same_v<out_data_t, float>)
		to_float32(in, out, n_samples, n_channels, in_datasize);
	else if constexpr (is_same_v<out_data_t, double>)
		to_float64(in, out, n_samples, n_channels, in_datasize);
}

template <typename in_data_t, typename out_data_t>
inline void converter(in_data_t* in, out_data_t** out, size_t n_samples, size_t n_channels,
	size_t in_datasize, [[maybe_unused]] size_t out_datasize)
{
	if constexpr (is_same_v<out_data_t, int8_t>)
		to_fixed8(in, out, n_samples, n_channels, in_datasize);		
	else if constexpr (is_same_v<out_data_t, int16_t>)
		to_fixed16(in, out, n_samples, n_channels, in_datasize);
	else if constexpr (is_same_v<out_data_t, int32_t>)
    {
		if (out_datasize == sizeof_fixed24)
			to_fixed24(in, out, n_samples, n_channels, in_datasize);
		else
			to_fixed32(in, out, n_samples, n_channels, in_datasize);
    }
	else if constexpr (is_same_v<out_data_t, float>)
		to_float32(in, out, n_samples, n_channels, in_datasize);
	else if constexpr (is_same_v<out_data_t, double>)
		to_float64(in, out, n_samples, n_channels, in_datasize);
}

error_code wave_reader_t::set_sample_index(size_t sample_index)
{
	if (sample_index > this->get_n_samples())
		return make_error_code(errc::invalid_seek);
	if (this->stream.is_open())
	{
		auto dst_pos = static_cast<streamoff>(sample_index * this->fmt.bits_per_sample * 8);
		auto cur_pos = static_cast<streamoff>(this->current_sample_index * 
			this->fmt.bits_per_sample * 8);
		this->stream.seekg(dst_pos - cur_pos, this->stream.cur);

		this->n_samples_remaining = this->get_n_samples() - sample_index;
		this->current_sample_index = sample_index;
		return error_code{};
	}
	return make_error_code(errc::io_error);
}

template error_code wave_reader_t::open(wave_read_set_t&);
template error_code wave_reader_t::open(const wave_read_set_t&);
template error_code wave_reader_t::open(wave_read_set_t&&);
template <typename ref_t>
error_code wave_reader_t::open(ref_t&& set)
{
	static_assert(is_same_v<decay_t<ref_t>, wave_read_set_t>, "wrong reference type");
	
	this->stream.open(set.path, ios::binary | ios::in);
	if (!this->stream.is_open())
	    this->open_stream_last_error = make_error_code(errc::io_error);
	else
	{
		this->stream.seekg(set.in_file_offset);
		this->open_stream_last_error = this->read_header();
		if (this->open_stream_last_error)
			return this->open_stream_last_error;
		this->n_samples_buffer_size = set.n_samples_buffer_size;
		this->open_stream_last_error = allocate_transition_buffer();
	}
	return this->open_stream_last_error;
}

error_code wave_reader_t::read_header()
{
	// read RIFF header
	chunk_RIFF_header_t RIFF;
	this->stream.read(reinterpret_cast<char*>(&RIFF), sizeof(RIFF));
#if defined(WAT_IS_BIG_ENDIAN_SYS)
	RIFF.head.chunk_size = _byteswap_ulong(RIFF.head.chunk_size);
#endif
	if (string(RIFF.head.chunk_id, sizeof(RIFF.head.chunk_id)) !=
		string(default_RIFF.head.chunk_id, sizeof(default_RIFF.head.chunk_id)))
		return make_error_code(errc::protocol_error);
	if (string(RIFF.format, sizeof(RIFF.format)) !=
		string(default_RIFF.format, sizeof(default_RIFF.format)))
		return make_error_code(errc::protocol_error);

	// read another
	streampos data_start_pos = 0;
	auto is_data = false;
	auto is_fmt = false;
	while (this->stream.tellg() < static_cast<streamoff>(RIFF.head.chunk_size))
	{
		subchunk_header_t head;
		this->stream.read(reinterpret_cast<char*>(&head), sizeof(head));
#if defined(WAT_IS_BIG_ENDIAN_SYS)
		head.chunk_size = _byteswap_ulong(head.chunk_size);
#endif
		if (string(head.chunk_id, sizeof(head.chunk_id)) ==
			string(default_fmt.head.chunk_id, sizeof(default_fmt.head.chunk_id)))
		{
			this->stream.read(reinterpret_cast<char*>(&fmt.format), sizeof(fmt) - sizeof(head));
			this->stream.seekg(head.chunk_size - sizeof(fmt) + sizeof(head), this->stream.cur);
			is_fmt = true;
#if defined(WAT_IS_BIG_ENDIAN_SYS)
			fmt_byteswap(&fmt);
#endif
			fmt.head = head;
		}
		else if (string(head.chunk_id, sizeof(head.chunk_id)) ==
			string(default_data.head.chunk_id, sizeof(default_data.head.chunk_id)))
		{
			this->data.head = head;
			data_start_pos = this->stream.tellg();
			this->stream.seekg(head.chunk_size, this->stream.cur);
			is_data = true;
		}
		else if (string(head.chunk_id, sizeof(head.chunk_id)) == "wavl")
		{
			return make_error_code(errc::not_supported);
		}
		else // unsupported subchunk
			this->stream.seekg(head.chunk_size, this->stream.cur);
	}
	if (!is_fmt || !is_data)
		return make_error_code(errc::protocol_error);
	
	uint16_t bytes_per_sample = this->fmt.bits_per_sample / 8;
	if ((bytes_per_sample != sizeof(int8_t)) && 
		(bytes_per_sample != sizeof(int16_t)) &&
		(bytes_per_sample != sizeof(sizeof_fixed24)) &&
		(bytes_per_sample != sizeof(int32_t)) &&
		(bytes_per_sample != sizeof(float)) &&
		(bytes_per_sample != sizeof(double)))
		return make_error_code(errc::not_supported);
	
	this->stream.seekg(data_start_pos);
	this->n_samples_remaining = static_cast<size_t>(this->data.head.chunk_size) /
		bytes_per_sample / this->fmt.n_channels;
	return error_code{};
}

template <typename out_data_t>
void wave_reader_t::read_samples(out_data_t* out, 
	size_t n_samples, uint16_t n_channels, size_t out_datasize)
{
	// do not care endianness
	char read_data[sizeof(int64_t)];
	auto hi_data_byte = &read_data[out_datasize - 1];
	for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
		for (uint16_t channel_index = 0; channel_index < n_channels; channel_index++)
		{
			this->stream.read(read_data, out_datasize);
			auto byte = hi_data_byte;
			if constexpr (is_integral_v<out_data_t>)
			{
				*out = static_cast<int32_t>(*byte--);
				if constexpr (sizeof(out_data_t) > sizeof(int8_t))
					for (size_t byte_index = 1; byte_index < out_datasize; byte_index++)
					{
						*out <<= 8;
						*out |= static_cast<out_data_t>(static_cast<uint8_t>(*byte--));
					}
			}
			else
			{
				if constexpr (sizeof(out_data_t) == sizeof(float))
				{
					auto sample = reinterpret_cast<int32_t*>(out);
					*sample = static_cast<int32_t>(*byte--);
					for (size_t byte_index = 1; byte_index < out_datasize; byte_index++)
					{
						*sample <<= 8;
						*sample |= static_cast<uint8_t>(*byte--);
					}
				}
				else if constexpr (sizeof(out_data_t) == sizeof(double))
				{
					auto sample = reinterpret_cast<int64_t*>(out);
					*sample = static_cast<int64_t>(*byte--);
					for (size_t byte_index = 1; byte_index < out_datasize; byte_index++)
					{
						*sample <<= 8;
						*sample |= static_cast<uint8_t>(*byte--);
					}
				}
			}
			out++;
		}
}

template error_code wave_reader_t::read<>(float*  , size_t, size_t);
template error_code wave_reader_t::read<>(double* , size_t, size_t);
template error_code wave_reader_t::read<>(int8_t* , size_t, size_t);
template error_code wave_reader_t::read<>(int16_t*, size_t, size_t);
template error_code wave_reader_t::read<>(int32_t*, size_t, size_t);
template <typename out_data_t>
error_code wave_reader_t::read(out_data_t* out, size_t n_samples, size_t out_datasize)
{
	size_t file_bytes_per_sample;
	auto error = this->state_check(n_samples, &file_bytes_per_sample);
	if (error)
		return error;

	if ((file_bytes_per_sample == sizeof(int8_t)) && (this->fmt.format == WAVE_FORMAT_PCM))
	{
		auto in = get<int8_t*>(this->transit_buffer.value());
		this->read_samples(in, n_samples, this->fmt.n_channels, sizeof(*in));
			
		// make signed samples
		auto sample = in;
		for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
			for (size_t channel_index = 0; channel_index < this->fmt.n_channels; channel_index++)
				*sample++ -= 128;

		converter(in, out, n_samples, this->fmt.n_channels, sizeof(*in), out_datasize);
	}
	else if ((file_bytes_per_sample == sizeof_fixed24) && (this->fmt.format == WAVE_FORMAT_PCM))
	{
		auto in = get<int32_t*>(this->transit_buffer.value());
		this->read_samples(in, n_samples, this->fmt.n_channels, sizeof_fixed24);

		// make sign extension for int32_t
		auto sample = in;
		for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
			for (size_t channel_index = 0; channel_index < this->fmt.n_channels; channel_index++)
			{
				*sample <<= 8; 
				*sample++ >>= 8;
			}
		converter(in, out, n_samples, this->fmt.n_channels, sizeof_fixed24, out_datasize);
	}
	else
	{
		visit([this, n_samples, out, out_datasize](auto&& buff_ptr)
			{
				this->read_samples(buff_ptr, n_samples, this->fmt.n_channels, sizeof(*buff_ptr));
				converter(buff_ptr, out, n_samples, this->fmt.n_channels, sizeof(*buff_ptr), out_datasize);
			}, 
			this->transit_buffer.value());
	}	
	return error_code{};
}

template error_code wave_reader_t::read<>(float**  , size_t, size_t);
template error_code wave_reader_t::read<>(double** , size_t, size_t);
template error_code wave_reader_t::read<>(int8_t** , size_t, size_t);
template error_code wave_reader_t::read<>(int16_t**, size_t, size_t);
template error_code wave_reader_t::read<>(int32_t**, size_t, size_t);
template <typename out_data_t>
error_code wave_reader_t::read(out_data_t** out, size_t n_samples, size_t out_datasize)
{
	size_t file_bytes_per_sample;
	auto error = this->state_check(n_samples, &file_bytes_per_sample);
	if (error)
		return error;

	if ((file_bytes_per_sample == sizeof(int8_t)) && (this->fmt.format == WAVE_FORMAT_PCM))
	{
		auto in = get<int8_t*>(this->transit_buffer.value());
		this->read_samples(in, n_samples, this->fmt.n_channels, sizeof(*in));
			
		// make signed samples
		auto sample = in;
		for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
			for (size_t channel_index = 0; channel_index < this->fmt.n_channels; channel_index++)
				*sample++ -= 128;

		converter(in, out, n_samples, this->fmt.n_channels, sizeof(*in), out_datasize);
	}
	else if ((file_bytes_per_sample == sizeof_fixed24) && (this->fmt.format == WAVE_FORMAT_PCM))
	{
		auto in = get<int32_t*>(this->transit_buffer.value());
		this->read_samples(in, n_samples, this->fmt.n_channels, sizeof_fixed24);

		// make sign extension for int32_t
		auto sample = in;
		for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
			for (size_t channel_index = 0; channel_index < this->fmt.n_channels; channel_index++)
			{
				*sample <<= 8; 
				*sample++ >>= 8;
			}
		converter(in, out, n_samples, this->fmt.n_channels, sizeof_fixed24, out_datasize);
	}
	else
	{
		visit([this, n_samples, out, out_datasize](auto&& buff_ptr)
			{
				this->read_samples(buff_ptr, n_samples, this->fmt.n_channels, sizeof(*buff_ptr));
				converter(buff_ptr, out, n_samples, this->fmt.n_channels, sizeof(*buff_ptr), out_datasize);
			}, 
			this->transit_buffer.value());
	}	
	return error_code{};
}

wave_encoding_tag wave_reader_t::get_format()
{
	return static_cast<wave_encoding_tag>(this->fmt.format);
}

wave_writer_t::~wave_writer_t()
{
	this->write_header(static_cast<streamsize>(this->current_sample_index * this->fmt.n_channels));
	if (this->stream.is_open())
		this->stream.flush();
}

template error_code wave_writer_t::open(wave_write_set_t&);
template error_code wave_writer_t::open(const wave_write_set_t&);
template error_code wave_writer_t::open(wave_write_set_t&&);
template <typename ref_t>
error_code wave_writer_t::open(ref_t&& set)
{
	static_assert(is_same_v<decay_t<ref_t>, wave_write_set_t>, "wrong reference type");

	this->stream.open(set.path, ios::binary | ios::trunc | ios::out);
	if (!this->stream.is_open())
		this->open_stream_last_error = make_error_code(errc::io_error);
	else
	{
		this->n_samples_buffer_size = set.n_samples_buffer_size;
		this->n_samples_remaining = (numeric_limits<uint32_t>::max() - 
			sizeof(this->fmt) - sizeof(subchunk_header_t) - sizeof(chunk_RIFF_header_t::format)) /
			this->fmt.bits_per_sample * 8 / this->fmt.n_channels;

		this->dealloc();
		this->fmt.n_channels = set.n_channels;
		this->fmt.sample_rate = set.samplerate;
		this->fmt.bits_per_sample = set.bits_per_sample;
		this->fmt.format = static_cast<uint16_t>(set.format);

		this->open_stream_last_error = this->write_header(0);
		if (this->open_stream_last_error)
			return this->open_stream_last_error;
		this->open_stream_last_error = this->allocate_transition_buffer();
	}
	return this->open_stream_last_error;
}

error_code wave_writer_t::write_header(streamsize data_size)
{
	this->n_samples_buffer_size = n_samples_buffer_size;
	auto original_position = stream.tellp();
	// go to beginning of the file
	this->stream.seekp(0);

	if (this->fmt.format == WAVE_FORMAT_PCM)
	{
		if (this->fmt.bits_per_sample != sizeof(int8_t) * 8 &&
			this->fmt.bits_per_sample != sizeof(int16_t) * 8 &&
			this->fmt.bits_per_sample != sizeof_fixed24 * 8 &&
			this->fmt.bits_per_sample != sizeof(int32_t) * 8)
			this->fmt.bits_per_sample = sizeof(int16_t) * 8;
	}
	else if (this->fmt.format == WAVE_FORMAT_IEEE_FLOAT)
	{
		if (this->fmt.bits_per_sample != sizeof(float) * 8 &&
			this->fmt.bits_per_sample != sizeof(double) * 8)
			this->fmt.bits_per_sample = sizeof(double) * 8;
	}
	// make headers
	auto RIFF = default_RIFF;
	uint16_t bytes_per_sample = this->fmt.bits_per_sample / 8;
	RIFF.head.chunk_size = static_cast<uint32_t>(sizeof(RIFF) + sizeof(this->fmt) + 
		sizeof(this->data) - sizeof(RIFF.head));
	RIFF.head.chunk_size += static_cast<uint32_t>(bytes_per_sample * data_size);
	// fmt header
	this->fmt.block_align = bytes_per_sample * this->fmt.n_channels;
	this->fmt.byte_rate = this->fmt.sample_rate * this->fmt.block_align;
	// data header
	this->data.head.chunk_size = static_cast<uint32_t>(data_size * bytes_per_sample);

#if defined(WAT_IS_BIG_ENDIAN_SYS)
	RIFF.head.chunk_size = _byteswap_ulong(RIFF.head.chunk_size);
	this->stream.write(reinterpret_cast<char*>(&RIFF), sizeof(RIFF));
	auto little_endian_fmt = this->fmt;
	this->fmt_byteswap(&little_endian_fmt);
	this->stream.write(reinterpret_cast<char*>(&little_endian_fmt), sizeof(little_endian_fmt));
	auto little_endian_data = this->data;
	little_endian_data.head.chunk_size = _byteswap_ulong(little_endian_data.head.chunk_size);
	this->stream.write(reinterpret_cast<char*>(&little_endian_data), sizeof(little_endian_data));
#else
	this->stream.write(reinterpret_cast<char*>(&RIFF), sizeof(RIFF));
	this->stream.write(reinterpret_cast<char*>(&this->fmt), sizeof(this->fmt));
	this->stream.write(reinterpret_cast<char*>(&this->data), sizeof(this->data));
#endif
	if (this->stream.fail())
		return make_error_code(errc::io_error);
	
	if (original_position > this->stream.tellp())
		this->stream.seekp(original_position);
	return error_code{};
}

template <typename in_data_t>
void wave_writer_t::write_samples(in_data_t* in, 
	size_t n_samples, uint16_t n_channels, size_t in_datasize)
{
	// do not care endianness
	char write_data[sizeof(int64_t)];
	for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
		for (uint16_t channel_index = 0; channel_index < n_channels; channel_index++)
		{
			auto out = write_data;
			if constexpr (is_integral_v<in_data_t>)
			{
				auto sample = *in;
				*out++ = static_cast<char>(sample);
				for (size_t byte_index = 1; byte_index < in_datasize; byte_index++)
				{
					sample >>= 8;
					*out++ = static_cast<char>(sample);
				}
			}
			else
			{
				auto sample = *reinterpret_cast<int64_t*>(in);
				*out++ = static_cast<char>(sample);
				for (size_t byte_index = 1; byte_index < in_datasize; byte_index++)
				{
					sample >>= 8;
					*out++ = static_cast<char>(sample);
				}
			}
			this->stream.write(write_data, in_datasize);
			in++;
		}
}

template error_code wave_writer_t::write<>(float*  , size_t, size_t);
template error_code wave_writer_t::write<>(double* , size_t, size_t);
template error_code wave_writer_t::write<>(int8_t* , size_t, size_t);
template error_code wave_writer_t::write<>(int16_t*, size_t, size_t);
template error_code wave_writer_t::write<>(int32_t*, size_t, size_t);
template <typename in_data_t>
error_code wave_writer_t::write(in_data_t* in, size_t n_samples, size_t in_datasize)
{
	size_t file_bytes_per_sample;
	auto error = this->state_check(n_samples, &file_bytes_per_sample);
	if (error)
		return error;

	if ((file_bytes_per_sample == sizeof(int8_t)) && (this->fmt.format == WAVE_FORMAT_PCM))
	{
		auto out = get<int8_t*>(this->transit_buffer.value());
		converter(in, out, n_samples, this->fmt.n_channels, in_datasize, sizeof(*out));

		// make unsigned samples
		auto sample = out;
		for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
			for (size_t channel_index = 0; channel_index < this->fmt.n_channels; channel_index++)
				*sample++ += 128;

		this->write_samples(out, n_samples, this->fmt.n_channels, sizeof(*out));
	}
	else if ((file_bytes_per_sample == sizeof_fixed24) && (this->fmt.format == WAVE_FORMAT_PCM))
	{
		auto out = get<int32_t*>(this->transit_buffer.value());
		converter(in, out, n_samples, this->fmt.n_channels, in_datasize, sizeof_fixed24);
		this->write_samples(out, n_samples, this->fmt.n_channels, sizeof_fixed24);
	}
	else
	{
		visit([this, in, n_samples, in_datasize](auto&& buff_ptr)
			{
				converter(in, buff_ptr, n_samples, this->fmt.n_channels, in_datasize, sizeof(*buff_ptr));
				this->write_samples(buff_ptr, n_samples, this->fmt.n_channels, sizeof(*buff_ptr));
			}, 
			this->transit_buffer.value());
	}
	return error_code{};
}

template error_code wave_writer_t::write<>(float**  , size_t, size_t);
template error_code wave_writer_t::write<>(double** , size_t, size_t);
template error_code wave_writer_t::write<>(int8_t** , size_t, size_t);
template error_code wave_writer_t::write<>(int16_t**, size_t, size_t);
template error_code wave_writer_t::write<>(int32_t**, size_t, size_t);
template <typename in_data_t>
error_code wave_writer_t::write(in_data_t** in, size_t n_samples, size_t in_datasize)
{
	size_t file_bytes_per_sample;
	auto error = this->state_check(n_samples, &file_bytes_per_sample);
	if (error)
		return error;

	if ((file_bytes_per_sample == sizeof(int8_t)) && (this->fmt.format == WAVE_FORMAT_PCM))
	{
		auto out = get<int8_t*>(this->transit_buffer.value());
		converter(in, out, n_samples, this->fmt.n_channels, in_datasize, sizeof(*out));

		// make unsigned samples
		auto sample = out;
		for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
			for (size_t channel_index = 0; channel_index < this->fmt.n_channels; channel_index++)
				*sample++ += 128;

		this->write_samples(out, n_samples, this->fmt.n_channels, sizeof(*out));
	}
	else if ((file_bytes_per_sample == sizeof_fixed24) && (this->fmt.format == WAVE_FORMAT_PCM))
	{
		auto out = get<int32_t*>(this->transit_buffer.value());
		converter(in, out, n_samples, this->fmt.n_channels, in_datasize, sizeof_fixed24);
		this->write_samples(out, n_samples, this->fmt.n_channels, sizeof_fixed24);
	}
	else
	{
		visit([this, in, n_samples, in_datasize](auto&& buff_ptr)
			{
				converter(in, buff_ptr, n_samples, this->fmt.n_channels, in_datasize, sizeof(*buff_ptr));
				this->write_samples(buff_ptr, n_samples, this->fmt.n_channels, sizeof(*buff_ptr));
			}, 
			this->transit_buffer.value());
	}
	return error_code{};
}

const chunk_RIFF_header_t default_RIFF =
{
	{{'R','I','F','F'}, 0},
	{'W','A','V','E'}
};
const subchunk_fmt_header_t default_fmt =
{
	{{'f','m','t',' '}, sizeof(subchunk_fmt_header_t) - sizeof(subchunk_header_t)},
	static_cast<uint16_t>(WAVE_FORMAT_PCM),
	1,
	44100,
	sizeof(int16_t) * 1 * 8,
	sizeof(int16_t) * 1,
	sizeof(int16_t) * 8
};
const subchunk_data_header_t default_data =
{
	{{'d','a','t','a'}, 0}
};
}
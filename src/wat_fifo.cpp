/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#include <cassert>
#include "wat_fifo.hpp"

namespace sea_wat
{
template class fifo_t<float>;
template class fifo_t<double>;
template class fifo_t<int8_t>;
template class fifo_t<int16_t>;
template class fifo_t<int32_t>;

template <typename data_t>
fifo_t<data_t>::fifo_t(size_t n_samples)
{
	this->storage = new data_t[n_samples];
	this->after_last_ptr = this->storage + n_samples;
	this->n_samples_size = n_samples;
	this->clear();
}

template <typename data_t>
fifo_t<data_t>::~fifo_t()
{
	delete[] this->storage;
}

template <typename data_t>
void fifo_t<data_t>::clear()
{
	this->write_ptr = this->storage;
	this->read_ptr = this->storage;
	this->n_samples_stored = 0;
}

template <typename data_t>
size_t fifo_t<data_t>::write_begin(data_t** in)
{
	auto n_samples_free = static_cast<size_t>(this->n_samples_size - this->n_samples_stored);
	auto n_samples_to_end = static_cast<size_t>(this->after_last_ptr - this->write_ptr);
	if (in)
	    *in = write_ptr;
	return n_samples_to_end < n_samples_free ? n_samples_to_end : n_samples_free;
}

template <typename data_t>
error_code fifo_t<data_t>::write_done(size_t n_samples)
{
	if (this->write_begin(nullptr) < n_samples)
		return make_error_code(errc::invalid_argument);

	auto new_write_ptr = this->write_ptr + n_samples;
	if (new_write_ptr >= this->after_last_ptr)
		this->write_ptr = this->storage;
	else
		this->write_ptr = new_write_ptr;
	this->n_samples_stored += n_samples;
	return error_code{};
}

template <typename data_t>
size_t fifo_t<data_t>::read_begin(data_t** out)
{
	auto n_samples_to_end = static_cast<size_t>(this->after_last_ptr - this->read_ptr);
	if (out)
		*out = read_ptr;
	return n_samples_to_end < this->n_samples_stored ? n_samples_to_end : this->n_samples_stored;
}

template <typename data_t>
error_code fifo_t<data_t>::read_done(size_t n_samples)
{
	if (this->n_samples_stored < n_samples)
		return make_error_code(errc::invalid_argument);

	auto new_read_ptr = this->read_ptr + n_samples;
	if (new_read_ptr >= this->after_last_ptr)
		this->read_ptr = this->storage;
	else 
		this->read_ptr = new_read_ptr;
	this->n_samples_stored -= n_samples;
	return error_code{};
}

template <typename data_t>
error_code fifo_t<data_t>::write(data_t* in, size_t n_samples, ptrdiff_t in_step)
{
	if (n_samples > this->n_samples_size)
		return make_error_code(errc::invalid_argument);
	if (this->write_ptr == this->read_ptr)
		if (this->n_samples_stored)
			return make_error_code(errc::invalid_argument);

	while (n_samples)
	{
		data_t* sample;
		auto n_samples_allowed = this->write_begin(&sample);
		if (n_samples < n_samples_allowed)
			n_samples_allowed = n_samples;
		for (size_t sample_index = 0; sample_index < n_samples_allowed; sample_index++)
		{
			*sample++ = *in;
			in += in_step;
		}
		this->write_done(n_samples_allowed);
		n_samples -= n_samples_allowed;
	}
	return error_code{};
}

template <typename data_t>
error_code fifo_t<data_t>::read(data_t* out, size_t n_samples, ptrdiff_t out_step)
{
	if (n_samples > this->n_samples_size)
		return make_error_code(errc::invalid_argument);
	if (this->write_ptr == this->read_ptr)
		if (!this->n_samples_stored)
			return make_error_code(errc::invalid_argument);
	if (this->n_samples_stored < n_samples)
		return make_error_code(errc::invalid_argument);

	while (n_samples)
	{
		data_t* sample;
		auto n_samples_allowed = this->read_begin(&sample);
		if (n_samples < n_samples_allowed)
			n_samples_allowed = n_samples;
		for (size_t sample_index = 0; sample_index < n_samples_allowed; sample_index++)
		{
			*out = *sample++;
			out += out_step;
		}
		this->read_done(n_samples_allowed);
		n_samples -= n_samples_allowed;
	}
	return error_code{};
}

template <typename data_t>
void fifo_t<data_t>::fill_value(data_t value)
{
	this->clear();
	auto sample = this->storage;
	for (size_t sample_index = 0; sample_index < this->n_samples_size; sample_index++)
		*sample++ = value;
	this->n_samples_stored = this->n_samples_size;
}

template <typename data_t>
data_t* fifo_t<data_t>::get_storage()
{
	return this->storage;
}

template <typename data_t>
size_t fifo_t<data_t>::get_n_samples_stored()
{
	return this->n_samples_stored;
}

template <typename data_t>
bool fifo_t<data_t>::is_full()
{
	return this->n_samples_size == this->n_samples_stored;
}
}
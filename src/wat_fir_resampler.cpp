/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#include <cassert>
#include <algorithm>
#if defined(WAT_OPENMP_ENABLE)
#include <omp.h>
#endif
#include "wat_primitives.hpp"
#include "wat_math.hpp"
#include "wat_fir_resampler.hpp"

namespace sea_wat
{

template class fir_resampler_t<float>;
template class fir_resampler_t<double>;
template class fir_resampler_t<int8_t>;
template class fir_resampler_t<int16_t>;
template class fir_resampler_t<int32_t>;

template <typename data_t>
fir_resampler_t<data_t>::fir_resampler_t(const fir_resampler_set_Hz_t& set) :
	fir_t<data_t>(fir_set_external_t {0, 0})
{
	this->init(set);
}

template <typename data_t>
fir_resampler_t<data_t>::fir_resampler_t() :
	fir_t<data_t>(fir_set_external_t{ 0, 0 })
{
	this->init(fir_resampler_default);
}

template <typename data_t>
void fir_resampler_t<data_t>::init(const fir_resampler_set_Hz_t& set)
{
	this->dealloc();
    this->n_channels = set.n_channels;
    auto f_samplerate = lcm(set.in_samplerate_Hz, set.out_samplerate_Hz);
    auto band_stop_begin = static_cast<float>(
		min(set.in_samplerate_Hz, set.out_samplerate_Hz) / 2 - 1) / f_samplerate;
    auto band_pass_end = band_stop_begin * 0.8f;
	this->int_scaler = static_cast<int32_t>(f_samplerate / set.in_samplerate_Hz);
	this->dec_scaler = static_cast<int32_t>(f_samplerate / set.out_samplerate_Hz);

	if constexpr (is_integral_v<data_t>)
	{	
		this->template make_response<int32_t>(0, band_pass_end, 
			band_stop_begin - band_pass_end, set.stop_band_gain_dB, linear_to_dB(this->int_scaler), 
			-70.f, band_pass, Kaiser_win);
		this->out_gain_shift = sizeof(int32_t) * 8 - 1;
	}
	else
	{
		this->template make_response<data_t>(0, band_pass_end, 
			band_stop_begin - band_pass_end, set.stop_band_gain_dB, linear_to_dB(this->int_scaler), 
			-70.f, band_pass, Kaiser_win);
		this->out_gain_shift = 0;
	}
	
	this->n_prev_virtual_samples = 0;
	this->n_in_sampes_per_length = this->length / this->int_scaler;
	// FIR filter make delay, therefore "start_delay_size" - is how many out_samples should be 
	// dropped on start. "flush_delay_size" - is how many samples should be saved in the end.
	// The first out_sample, calculated from the prev_samples, is allways = 0 and does not calculate. 
	// It gives "-1" to virtual FIR length ----------------------------------------------------> here
	this->start_delay_size = (this->n_in_sampes_per_length * this->int_scaler / this->dec_scaler - 1) / 2;
	this->flush_delay_size = this->start_delay_size;	// TODO: check with even length
    this->filter_tail = this->length % this->int_scaler;
	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		this->prev_samples.push_back(new data_t[this->n_in_sampes_per_length]);
		auto sample = static_cast<data_t*>(this->prev_samples.back());
		for (size_t sample_index = 0; sample_index < this->n_in_sampes_per_length; sample_index++)
			*sample++ = static_cast<data_t>(0);
	}
}

template <typename data_t>
fir_resampler_t<data_t>::~fir_resampler_t()
{
}

template <typename data_t>
size_t fir_resampler_t<data_t>::process(data_t* in, data_t* out, size_t n_samples, bool out_add)
{
	vector<data_t*> in_vec, out_vec;
	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		in_vec.push_back(in + channel_index);
		out_vec.push_back(out + channel_index);
	}
	auto make_out = out_add ? make_out_add<data_t> : make_out_move<data_t>;
	if constexpr (is_integral_v<data_t>)
		return this->process<int32_t>(
			in_vec, out_vec, n_samples, this->n_channels, this->n_channels, make_out);
	else
	{
		if constexpr (is_same_v<data_t, float>)
			return this->process<float>(
				in_vec, out_vec, n_samples, this->n_channels, this->n_channels, make_out);
		else if constexpr (is_same_v<data_t, double>)
			return this->process<double>(
				in_vec, out_vec, n_samples, this->n_channels, this->n_channels, make_out);
	}
}

template <typename data_t>
size_t fir_resampler_t<data_t>::process(data_t** in, data_t** out, size_t n_samples, bool out_add)
{
	vector<data_t*> in_vec, out_vec;
	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		in_vec.push_back(in[channel_index]);
		out_vec.push_back(out[channel_index]);
	}
	auto make_out = out_add ? make_out_add<data_t> : make_out_move<data_t>;
	if constexpr (is_integral_v<data_t>)
		return this->process<int32_t>(in_vec, out_vec, n_samples, 1, 1, make_out);
	else
	{
		if constexpr (is_same_v<data_t, float>)
			return this->process<float>(in_vec, out_vec, n_samples, 1, 1, make_out);
		else if constexpr (is_same_v<data_t, double>)
			return this->process<double>(in_vec, out_vec, n_samples, 1, 1, make_out);
	}
}

template <typename acc_t, typename data0_t, typename data1_t>
inline void arrays_mac(data0_t** arr0, data1_t** arr1, size_t n_samples,
	ptrdiff_t in0_step, ptrdiff_t in1_step, acc_t* acc)
{
	for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
	{
		*acc += static_cast<acc_t>(**arr0) * static_cast<acc_t>(**arr1);
		*arr0 += in0_step;
		*arr1 += in1_step;
	}
}

template size_t fir_resampler_t<float>  ::process<float>  (const vector<float*>&,   const vector<float*>&, 
	size_t, ptrdiff_t, ptrdiff_t, function<make_out_t<float>>&);
template size_t fir_resampler_t<double> ::process<double> (const vector<double*>&,  const vector<double*>&, 
	size_t, ptrdiff_t, ptrdiff_t, function<make_out_t<double>>&);
template size_t fir_resampler_t<int8_t> ::process<int32_t>(const vector<int8_t*>&,  const vector<int8_t*>&, 
	size_t, ptrdiff_t, ptrdiff_t, function<make_out_t<int8_t>>&);
template size_t fir_resampler_t<int16_t>::process<int32_t>(const vector<int16_t*>&, const vector<int16_t*>&, 
	size_t, ptrdiff_t, ptrdiff_t, function<make_out_t<int16_t>>&);
template size_t fir_resampler_t<int32_t>::process<int32_t>(const vector<int32_t*>&, const vector<int32_t*>&, 
	size_t, ptrdiff_t, ptrdiff_t, function<make_out_t<int32_t>>&);
template <typename data_t>
template <typename coeff_t>
size_t fir_resampler_t<data_t>::process(const vector<data_t*>& in, const vector<data_t*>& out, 
	size_t n_samples, ptrdiff_t in_step, ptrdiff_t out_step, function<make_out_t<data_t>>& make_out)
{
	assert(in.size() == this->n_channels);
	assert(out.size() == this->n_channels);
	size_t n_samples_to_out = 
		(n_samples * this->int_scaler + this->n_prev_virtual_samples) / this->dec_scaler;
	auto n_samples_to_save = 
		this->n_in_sampes_per_length < n_samples ? this->n_in_sampes_per_length : n_samples;
	size_t n_prev_samples_to_keep = 0;
	if (n_samples_to_save < this->n_in_sampes_per_length)
		n_prev_samples_to_keep = this->n_in_sampes_per_length - n_samples_to_save;

	// indexes of output samples in the virtual buffer
	vector<size_t> output_indexes = 
	{
		static_cast<size_t>(this->dec_scaler - this->n_prev_virtual_samples - 1)
	};
	for (size_t out_sample_index = 1; out_sample_index < n_samples_to_out; out_sample_index++)
		output_indexes.push_back(output_indexes.back() + this->dec_scaler);

#if defined(WAT_OPENMP_ENABLE)
#pragma omp parallel for
#endif
	for (size_t out_sample_index = 0; out_sample_index < n_samples_to_out; out_sample_index++)
	{
		// find the first input data index for current output sample
		auto output_index = output_indexes[out_sample_index];
		auto in_sample_index_first = static_cast<ptrdiff_t>(output_index / this->int_scaler);
		size_t n_current_samples = 1;
		if (in_sample_index_first)
		{
			n_current_samples += in_sample_index_first;
			if (n_current_samples > this->n_in_sampes_per_length)
				n_current_samples = this->n_in_sampes_per_length;
		}
		auto n_prev_samples = this->n_in_sampes_per_length - n_current_samples;
		auto first_coeff = &static_cast<coeff_t*>(this->filter_model)[output_index % this->int_scaler];
		// is extra sample needed
		if (output_index % this->int_scaler < this->filter_tail)
		{
			if (in_sample_index_first - static_cast<ptrdiff_t>(n_current_samples) < 0)
				n_prev_samples += 1;
			else
				n_current_samples += 1;
		}
		auto out_sample_pos = out_sample_index * out_step;
		auto in_sample_pos = in_sample_index_first * in_step;
		auto in_for_channel = in.begin();
		auto prev_samples_for_channel = this->prev_samples.begin();
		for (auto out_for_channel = out.begin(); out_for_channel < out.end(); out_for_channel++)
		{
			auto coeff = first_coeff;
			auto in_sample = &(*in_for_channel)[in_sample_pos];
			auto in_sample_prev = static_cast<data_t*>(*prev_samples_for_channel);
			if constexpr (is_integral_v<data_t>)
			{
				auto acc = static_cast<int64_t>(0);
				arrays_mac<int64_t>(&in_sample, &coeff, n_current_samples, -in_step, this->int_scaler, &acc);
				arrays_mac<int64_t>(&in_sample_prev, &coeff, n_prev_samples, 1, this->int_scaler, &acc);
				make_out((*out_for_channel)[out_sample_pos], 
					static_cast<data_t>(acc >> this->out_gain_shift));
			}
			else
			{
				auto acc = static_cast<data_t>(0);
				arrays_mac<data_t>(&in_sample, &coeff, n_current_samples, -in_step, this->int_scaler, &acc);
				arrays_mac<data_t>(&in_sample_prev, &coeff, n_prev_samples, 1, this->int_scaler, &acc);
				make_out((*out_for_channel)[out_sample_pos], acc);
			}
			in_for_channel++;
			prev_samples_for_channel++;
		}
	}

	auto prev_samples_for_channel = this->prev_samples.begin();
#if defined(WAT_OPENMP_ENABLE)
#pragma omp parallel for
#endif
	for (auto in_for_channel = in.begin(); in_for_channel < in.end(); in_for_channel++)
	{
		// keep the newest samples and move them to the tail
		auto save_out = static_cast<data_t*>(*prev_samples_for_channel) + 
			this->n_in_sampes_per_length - 1;
		if (n_prev_samples_to_keep)
		{
			auto save_in = static_cast<data_t*>(*prev_samples_for_channel) + 
				n_prev_samples_to_keep - 1;
			for (size_t sample_index = 0; sample_index < n_prev_samples_to_keep; sample_index++)
				*save_out-- = *save_in--;
		}
		// save the tail of samples to the local memory
		auto save_in = &(*in_for_channel)[(n_samples - n_samples_to_save) * in_step];
		for (size_t sample_index = 0; sample_index < n_samples_to_save; sample_index++)
		{
			*save_out-- = *save_in;
			save_in += in_step;
		}
		prev_samples_for_channel++;
	}
	// start delay compensation
	if (this->start_delay_size)
	{
		if (this->start_delay_size < n_samples_to_out)
		{
			for (auto out_for_channel = out.begin(); out_for_channel < out.end(); out_for_channel++)
			{
				auto save_out = *out_for_channel;
				auto save_in = &(*out_for_channel)[this->start_delay_size * out_step];
				for (size_t sample_index = 0; 
					sample_index < (n_samples_to_out - this->start_delay_size); sample_index++)
				{
					*save_out = *save_in;
					save_out += out_step;
					save_in += out_step;
				}
			}
			n_samples_to_out -= this->start_delay_size;
			this->start_delay_size = 0;
		}
		else
		{
			this->start_delay_size -= n_samples_to_out;
			n_samples_to_out = 0;
		}
	}
	// the tail of samples, for the next buffer processing
	this->n_prev_virtual_samples = 
		(n_samples * this->int_scaler + this->n_prev_virtual_samples) % this->dec_scaler;
	return n_samples_to_out;
}

template <typename data_t>
size_t fir_resampler_t<data_t>::get_flush_size()
{
	return this->flush_delay_size;
}

template <typename data_t>
size_t fir_resampler_t<data_t>::flush(data_t* out, size_t n_out_samples, bool out_add)
{
	if (n_out_samples)
	{
		data_t flush_buff = 0;
		vector<data_t*> flush_vec, out_vec;
		for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
		{
			flush_vec.push_back(&flush_buff);
			out_vec.push_back(out + channel_index);
		}
		auto n_in_samples = this->required_in_for_n_out_samples(n_out_samples);
		if (this->flush_delay_size >= n_out_samples)
			this->flush_delay_size -= n_out_samples;
		else 
			this->flush_delay_size = 0;
		auto make_out = out_add ? make_out_add<data_t> : make_out_move<data_t>;
		if constexpr (is_integral_v<data_t>)
			return this->process<int32_t>(
				flush_vec, out_vec, n_in_samples, 0, this->n_channels, make_out);
		else
		{
			if constexpr (sizeof(data_t) == sizeof(float))
				return this->process<float>(
					flush_vec, out_vec, n_in_samples, 0, this->n_channels, make_out);
			else if constexpr (sizeof(data_t) == sizeof(double))
				return this->process<double>(
					flush_vec, out_vec, n_in_samples, 0, this->n_channels, make_out);
		}
	}
	else
		return 0;
}

template <typename data_t>
size_t fir_resampler_t<data_t>::flush(data_t** out, size_t n_out_samples, bool out_add)
{
	if (n_out_samples)
	{
		data_t flush_buff = 0;
		vector<data_t*> flush_vec, out_vec;
		for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
		{
			flush_vec.push_back(&flush_buff);
			out_vec.push_back(out[channel_index]);
		}
		auto n_in_samples = this->required_in_for_n_out_samples(n_out_samples);
		if (this->flush_delay_size >= n_out_samples)
			this->flush_delay_size -= n_out_samples;
		else
			this->flush_delay_size = 0;
		auto make_out = out_add ? make_out_add<data_t> : make_out_move<data_t>;
		if constexpr (is_integral_v<data_t>)
			return this->process<int32_t>(flush_vec, out_vec, n_in_samples, 0, 1, make_out);
		else
		{
			if constexpr (sizeof(data_t) == sizeof(float))
				return this->process<float>(flush_vec, out_vec, n_in_samples, 0, 1, make_out);
			else if constexpr (sizeof(data_t) == sizeof(double))
				return this->process<double>(flush_vec, out_vec, n_in_samples, 0, 1, make_out);
		}
	}
	else
		return 0;
}

template <typename data_t>
inline size_t fir_resampler_t<data_t>::required_in_for_n_out_samples(size_t n_out_samples)
{
	return static_cast<size_t>(ceil(static_cast<float>(
		n_out_samples * this->dec_scaler - this->n_prev_virtual_samples) / this->int_scaler));
}

const fir_resampler_set_Hz_t fir_resampler_default = { 44100, 48000, 1, -70.f };
}
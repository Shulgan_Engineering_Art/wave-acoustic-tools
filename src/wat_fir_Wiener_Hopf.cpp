﻿/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools.
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#include <cassert>
#include <cstring>
#include "wat_fir_Wiener_Hopf.hpp"

namespace sea_wat
{

template class fir_Wiener_Hopf_t<float>;
template class fir_Wiener_Hopf_t<double>;
template class fir_Wiener_Hopf_t<int8_t>;
template class fir_Wiener_Hopf_t<int16_t>;
template class fir_Wiener_Hopf_t<int32_t>;

template <typename data_t>
fir_Wiener_Hopf_t<data_t>::fir_Wiener_Hopf_t(const fir_Wiener_Hopf_set_t& set) :
	fir_t<data_t>(fir_set_external_t{ 0, 0 })
{
	this->init(set);
}

template <typename data_t>
fir_Wiener_Hopf_t<data_t>::fir_Wiener_Hopf_t(const fir_Wiener_Hopf_set_Hz_t& set) :
	fir_t<data_t>(fir_set_external_t{ 0, 0 })
{
	assert(set.band_begin_Hz < set.samplerate_Hz);
	assert(set.band_end_Hz < set.samplerate_Hz);

	// ......
}

template <typename data_t>
fir_Wiener_Hopf_t<data_t>::fir_Wiener_Hopf_t() :
	fir_t<data_t>(fir_set_external_t{ 0, 0 })
{
	this->init(fir_Wiener_Hopf_default);
}

template <typename data_t>
fir_Wiener_Hopf_t<data_t>::~fir_Wiener_Hopf_t()
{
	this->dealloc();
}

template <typename data_t>
void fir_Wiener_Hopf_t<data_t>::init(const fir_Wiener_Hopf_set_t& set)
{
	this->dealloc();
	this->n_channels = set.n_channels;

	// make the first channel's initial model (response)
	if constexpr (is_integral_v<data_t>)
	{
		/*this->length = 300;
		this->template make_response<float>(set.band_begin, set.band_end,
			0.f, set.stop_band_gain_dB, set.pass_band_gain_dB,
			set.pass_band_ripple_dB, set.fir_type, set.fir_response_type);

		auto coeff = static_cast<float*>(this->filter_model);
		for (size_t coeff_index = 0; coeff_index < this->length; coeff_index++)
			*coeff++ = 0.f;*/

		// make int64_t !!!!
		/*this->template make_response<float>(set.band_begin, set.band_end,
			set.transition_width, set.stop_band_gain_dB, set.pass_band_gain_dB,
			set.pass_band_ripple_dB, set.fir_type, set.fir_response_type);*/

		this->models.push_back(this->filter_model);
		// another channel are the same on start
		for (size_t channel_index = 1; channel_index < this->n_channels; channel_index++)
		{
			this->models.push_back(new float[this->length]);
			memcpy(this->models.back(), this->filter_model, this->length * sizeof(int32_t));
		}
	}
	else
	{
		this->template make_response<data_t>(set.band_begin, set.band_end,
			set.transition_width, set.stop_band_gain_dB, set.pass_band_gain_dB,
			set.pass_band_ripple_dB, set.fir_type, set.fir_response_type);

		this->models.push_back(this->filter_model);
		// another channel are the same on start
		for (size_t channel_index = 1; channel_index < this->n_channels; channel_index++)
		{
			this->models.push_back(new data_t[this->length]);
			memcpy(this->models.back(), this->filter_model, this->length * sizeof(data_t));
		}
	}
	
	this->filter_model = nullptr;
	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		this->prev_samples.push_back(new data_t[this->length]);
		auto sample = static_cast<data_t*>(this->prev_samples.back());
		for (size_t sample_index = 0; sample_index < this->length; sample_index++)
			*sample++ = static_cast<data_t>(0);
	}
}

template <typename data_t>
size_t fir_Wiener_Hopf_t<data_t>::process(data_t* in, data_t* in_ref, data_t* out_err, size_t n_samples, bool out_add)
{
	vector<data_t*> in_vec, in_ref_vec, out_vec;
	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		in_vec.push_back(in + channel_index);
		in_ref_vec.push_back(in_ref + channel_index);
		out_vec.push_back(out_err + channel_index);
	}
	function<void(data_t & out, data_t & in)> make_out;
	if (out_add)
		make_out = [](data_t& out, data_t& in) {out += in; };
	else
		make_out = [](data_t& out, data_t& in) {out = in; };
	if constexpr (is_integral_v<data_t>)
		return this->process<int32_t>(in_vec, in_ref_vec, out_vec, n_samples,
			this->n_channels, this->n_channels, this->n_channels, make_out);
	else
		if constexpr (is_same_v<data_t, float>)
			return this->process<float>(in_vec, in_ref_vec, out_vec, n_samples,
				this->n_channels, this->n_channels, this->n_channels, make_out);
		else if constexpr (is_same_v<data_t, double>)
			return this->process<double>(in_vec, in_ref_vec, out_vec, n_samples,
				this->n_channels, this->n_channels, this->n_channels, make_out);
}

template <typename data_t>
size_t fir_Wiener_Hopf_t<data_t>::process(data_t** in, data_t** in_ref, data_t** out_err, size_t n_samples, bool out_add)
{
	vector<data_t*> in_vec, in_ref_vec, out_vec;
	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		in_vec.push_back(in[channel_index]);
		in_ref_vec.push_back(in_ref[channel_index]);
		out_vec.push_back(out_err[channel_index]);
	}
	function<void(data_t & out, data_t & in)> make_out;
	if (out_add)
		make_out = [](data_t& out, data_t& in) {out += in; };
	else
		make_out = [](data_t& out, data_t& in) {out = in; };
	if constexpr (is_integral_v<data_t>)
		return this->process<int32_t>(in_vec, in_ref_vec, out_vec, n_samples, 1, 1, 1, make_out);
	else
		if constexpr (is_same_v<data_t, float>)
			return this->process<float>(in_vec, in_ref_vec, out_vec, n_samples, 1, 1, 1, make_out);
		else if constexpr (is_same_v<data_t, double>)
			return this->process<double>(in_vec, in_ref_vec, out_vec, n_samples, 1, 1, 1, make_out);
}

template <typename acc_t, typename data0_t, typename data1_t>
inline void arrays_mac(data0_t** arr0, data1_t** arr1, size_t n_samples,
	ptrdiff_t in0_step, ptrdiff_t in1_step, acc_t* acc)
{
	for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
	{
		*acc += static_cast<acc_t>(**arr0) * static_cast<acc_t>(**arr1);
		*arr0 += in0_step;
		*arr1 += in1_step;
	}
}

template size_t fir_Wiener_Hopf_t<float>  ::process<float>(const vector<float*>&, const vector<float*>&,
	const vector<float*>&, size_t, ptrdiff_t, ptrdiff_t, ptrdiff_t, function<void(float&, float&)>&);
template size_t fir_Wiener_Hopf_t<double> ::process<double>(const vector<double*>&, const vector<double*>&,
	const vector<double*>&, size_t, ptrdiff_t, ptrdiff_t, ptrdiff_t, function<void(double&, double&)>&);
template size_t fir_Wiener_Hopf_t<int8_t> ::process<int32_t>(const vector<int8_t*>&, const vector<int8_t*>&,
	const vector<int8_t*>&, size_t, ptrdiff_t, ptrdiff_t, ptrdiff_t, function<void(int8_t&, int8_t&)>&);
template size_t fir_Wiener_Hopf_t<int16_t> ::process<int32_t>(const vector<int16_t*>&, const vector<int16_t*>&,
	const vector<int16_t*>&, size_t, ptrdiff_t, ptrdiff_t, ptrdiff_t, function<void(int16_t&, int16_t&)>&);
template size_t fir_Wiener_Hopf_t<int32_t> ::process<int32_t>(const vector<int32_t*>&, const vector<int32_t*>&,
	const vector<int32_t*>&, size_t, ptrdiff_t, ptrdiff_t, ptrdiff_t, function<void(int32_t&, int32_t&)>&);
template <typename data_t>
template <typename coeff_t>
size_t fir_Wiener_Hopf_t<data_t>::process(const vector<data_t*>& in, const vector<data_t*>& in_ref, const vector<data_t*>& out_err,
	size_t n_samples, ptrdiff_t in_step, ptrdiff_t in_ref_step, ptrdiff_t out_step,
	function<void(data_t&, data_t&)>& make_out)
{
	assert(in.size() == this->n_channels);
	assert(in_ref.size() == this->n_channels);
	assert(out_err.size() == this->n_channels);

	auto n_samples_to_save = this->length < n_samples ? this->length : n_samples;
	size_t n_prev_samples_to_keep = 0;
	if (n_samples_to_save < this->length)
		n_prev_samples_to_keep = this->length - n_samples_to_save;

	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		auto sample_in = in[channel_index];
		auto sample_out_err = out_err[channel_index];
		for (size_t out_sample_index = 0; out_sample_index < n_samples; out_sample_index++)
		{
			// find expected signal from the reference
			size_t n_prev_samples = 0;
			size_t n_current_samples = out_sample_index + 1;
			if (this->length > out_sample_index)
				n_prev_samples = this->length - n_current_samples;
			else
				n_current_samples = this->length;

			auto coeff = static_cast<coeff_t*>(this->models[channel_index]);
			auto ref_sample = in_ref[channel_index] + out_sample_index * in_ref_step;
			
			if constexpr (is_integral_v<data_t>)
			{
				auto acc = static_cast<int64_t>(0);
				for (size_t sample_index = 0; sample_index < n_current_samples; sample_index++)
				{
					acc += *ref_sample * *coeff++;
					ref_sample -= in_ref_step;
				}
				//arrays_mac<int64_t>(&ref_sample, &coeff, n_current_samples, -in_ref_step, 1, &acc);
				ref_sample = static_cast<data_t*>(this->prev_samples[channel_index]);
				for (size_t sample_index = 0; sample_index < n_prev_samples; sample_index++)
				{
					acc += *ref_sample++ * *coeff++;
				}
				//arrays_mac<int64_t>(&ref_sample, &coeff, n_prev_samples, 1, 1, &acc);
				// difference compensation, find error and correction coeff
				*sample_out_err = *sample_in - static_cast<data_t>(acc >> 32);
			}
			else
			{
				auto acc = static_cast<data_t>(0);
				arrays_mac<data_t>(&ref_sample, &coeff, n_current_samples, -in_ref_step, 1, &acc);
				ref_sample = static_cast<data_t*>(this->prev_samples[channel_index]);
				arrays_mac<data_t>(&ref_sample, &coeff, n_prev_samples, 1, 1, &acc);
				// difference compensation, find error and correction coeff
				*sample_out_err = *sample_in - static_cast<data_t>(acc);
			}
			ref_sample = in_ref[channel_index] + out_sample_index * in_ref_step;
			// make correction
			if (*ref_sample != static_cast<data_t>(0))
			{
				auto correction_coeff = static_cast<coeff_t>(2 * 0.00002 * *sample_out_err);
				coeff = static_cast<coeff_t*>(this->models[channel_index]);
				for (size_t sample_index = 0; sample_index < n_current_samples; sample_index++)
				{
					*coeff++ += correction_coeff * *ref_sample;
					ref_sample -= in_ref_step;
				}
				ref_sample = static_cast<data_t*>(this->prev_samples[channel_index]);
				for (size_t sample_index = 0; sample_index < n_prev_samples; sample_index++)
				{
					*coeff++ += correction_coeff * *ref_sample++;
				}
			}
			sample_in += in_step;
			sample_out_err += out_step;
		}
	}

	auto prev_samples_for_channel = this->prev_samples.begin();
#if defined(WAT_OPENMP_ENABLE)
	//#pragma omp parallel for
#endif
	for (auto in_for_channel = in_ref.begin(); in_for_channel < in_ref.end(); in_for_channel++)
	{
		// keep the newest samples and move them to the tail
		auto save_out = static_cast<data_t*>(*prev_samples_for_channel) + this->length - 1;
		if (n_prev_samples_to_keep)
		{
			auto save_in = static_cast<data_t*>(*prev_samples_for_channel) +
				n_prev_samples_to_keep - 1;
			for (size_t sample_index = 0; sample_index < n_prev_samples_to_keep; sample_index++)
				*save_out-- = *save_in--;
		}
		// save the tail of samples to the local memory
		auto save_in = &(*in_for_channel)[(n_samples - n_samples_to_save) * in_ref_step];
		for (size_t sample_index = 0; sample_index < n_samples_to_save; sample_index++)
		{
			*save_out-- = *save_in;
			save_in += in_ref_step;
		}
		prev_samples_for_channel++;
	}

	return n_samples;
}

template <typename data_t>
void* fir_Wiener_Hopf_t<data_t>::get_model(size_t channel_index)
{
	if (channel_index >= this->models.size())
		return nullptr;
	return this->models[channel_index];
}

template <typename data_t>
void fir_Wiener_Hopf_t<data_t>::dealloc()
{
	for (auto channel = this->models.begin(); channel < this->models.end(); channel++)
		delete *channel;
	this->models.clear();
}


const fir_Wiener_Hopf_set_t fir_Wiener_Hopf_default = { band_pass, rectangular, 1, 0.1f, 0.2f, 0.00015f, -70.f, 0.f, };
}
/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#include <limits>
#include <cmath>
#include <vector>
#include <type_traits>
#include "wat_primitives.hpp"

namespace sea_wat
{

const int32_t max_value_fixed24 = (1 << 23) - 1;
const int32_t min_value_fixed24 = -(1 << 23);
const float max_value_float = 1.f;
const float min_value_float = -1.f;
const size_t sizeof_fixed24 = 3;

template float   max_datatype_value<>(size_t in_datasize);
template double  max_datatype_value<>(size_t in_datasize);
template int8_t  max_datatype_value<>(size_t in_datasize);
template int16_t max_datatype_value<>(size_t in_datasize);
template int32_t max_datatype_value<>(size_t in_datasize);
template <typename data_t>
data_t max_datatype_value([[maybe_unused]] size_t in_datasize)
{
	if constexpr (is_integral_v<data_t>)
	{
		int64_t result = numeric_limits<data_t>::max();
		if (in_datasize == sizeof_fixed24)
			result = max_value_fixed24;
		return static_cast<data_t>(result);
	}
	else
		return max_value_float;
}

template <typename data_t>
data_t min_datatype_value(size_t in_datasize = sizeof(data_t))
{
	if constexpr (is_integral_v<data_t>)
	{
		int64_t result = numeric_limits<data_t>::min();
		if (in_datasize == sizeof_fixed24)
			result = min_value_fixed24;
		return static_cast<data_t>(result);
	}
	else
		return min_value_float;
}

template <typename in_data_t, typename out_data_t>
inline void fixed_point_converter_interleaver_deinterleaver(in_data_t* __restrict in,
    out_data_t* __restrict out, size_t n_samples, ptrdiff_t in_step, ptrdiff_t out_step,
	[[maybe_unused]] ptrdiff_t shift, [[maybe_unused]] size_t out_datasize, 
	function<make_out_t<out_data_t>>& make_out)
{
	if constexpr (is_integral_v<in_data_t>)
	{
		if (shift < 0)
			for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
			{
				make_out(*out, static_cast<out_data_t>(*in >> -shift));
				in += in_step;
				out += out_step;
			}
		else
			for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
			{
				make_out(*out, static_cast<out_data_t>(*in) << shift);
				in += in_step;
				out += out_step;
			}
	}
	else
	{
		auto min_limith = min_datatype_value<out_data_t>(out_datasize);
		auto max_limith = max_datatype_value<out_data_t>(out_datasize);
		for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
		{
			auto value = static_cast<int64_t>(*in * max_limith);
			if (value < min_limith)
				value = min_limith;
			else if (value > max_limith)
				value = max_limith;
			make_out(*out, static_cast<out_data_t>(value));
			in += in_step;
			out += out_step;
		}
	}
}

template <typename in_data_t, typename out_data_t>
inline void float_point_converter_interleaver_deinterleaver(in_data_t* __restrict in,
        out_data_t* __restrict out, size_t n_samples, ptrdiff_t in_step, ptrdiff_t out_step,
		[[maybe_unused]] int64_t divider, function<make_out_t<out_data_t>>& make_out)
{
	for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
	{
		if constexpr (is_integral_v<in_data_t>)
			make_out(*out, static_cast<float>(*in) / divider);
		else
			make_out(*out, static_cast<out_data_t>(*in));
		in += in_step;
		out += out_step;
	}
}

template <typename in_data_t, typename out_data_t>
void converter_interleaver_deinterleaver(in_data_t* in, out_data_t* out, size_t n_samples, 
	size_t n_channels, size_t in_datasize, [[maybe_unused]] size_t out_datasize, bool out_add)
{
	auto make_out = out_add ? make_out_add<out_data_t> : make_out_move<out_data_t>;
	if constexpr (is_integral_v<out_data_t>)
	{
		auto shift = (static_cast<ptrdiff_t>(out_datasize) - static_cast<ptrdiff_t>(in_datasize)) * 8;
		for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
			fixed_point_converter_interleaver_deinterleaver(
				in + channel_index,
				out + channel_index,
				n_samples,
				n_channels,
				n_channels,
				shift,
				out_datasize,
				make_out);
	}
	else
	{
		auto gain_scaler = static_cast<int64_t>(max_datatype_value<in_data_t>(in_datasize));
		for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
			float_point_converter_interleaver_deinterleaver(
				in + channel_index,
				out + channel_index,
				n_samples,
				n_channels,
				n_channels,
				gain_scaler,
				make_out);
	}
}

template <typename in_data_t, typename out_data_t>
void converter_interleaver_deinterleaver(in_data_t** in, out_data_t** out, size_t n_samples, 
	size_t n_channels, size_t in_datasize, [[maybe_unused]] size_t out_datasize, bool out_add)
{
	auto make_out = out_add ? make_out_add<out_data_t> : make_out_move<out_data_t>;
	if constexpr (is_integral_v<out_data_t>)
	{
		auto shift = (static_cast<ptrdiff_t>(out_datasize) - static_cast<ptrdiff_t>(in_datasize)) * 8;
		for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
			fixed_point_converter_interleaver_deinterleaver(
				in[channel_index],
				out[channel_index],
				n_samples,
				1,
				1,
				shift,
				out_datasize,
				make_out);
	}
	else
	{
		auto gain_scaler = static_cast<int64_t>(max_datatype_value<in_data_t>(in_datasize));
		for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
			float_point_converter_interleaver_deinterleaver(
				in[channel_index],
				out[channel_index],
				n_samples,
				1,
				1,
				gain_scaler,
				make_out);
	}
}

template <typename in_data_t, typename out_data_t>
void converter_interleaver_deinterleaver(in_data_t** in, out_data_t* out, size_t n_samples, 
	size_t n_channels, size_t in_datasize, [[maybe_unused]] size_t out_datasize, bool out_add)
{
	auto make_out = out_add ? make_out_add<out_data_t> : make_out_move<out_data_t>;
	if constexpr (is_integral_v<out_data_t>)
	{
		auto shift = (static_cast<ptrdiff_t>(out_datasize) - static_cast<ptrdiff_t>(in_datasize)) * 8;
		for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
			fixed_point_converter_interleaver_deinterleaver(
				in[channel_index],
				out + channel_index,
				n_samples,
				1,
				n_channels,
				shift,
				out_datasize,
				make_out);
	}
	else
	{
		auto gain_scaler = static_cast<int64_t>(max_datatype_value<in_data_t>(in_datasize));
		for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
			float_point_converter_interleaver_deinterleaver(
				in[channel_index],
				out + channel_index,
				n_samples,
				1,
				n_channels,
				gain_scaler,
				make_out);
	}
}

template <typename in_data_t, typename out_data_t>
void converter_interleaver_deinterleaver(in_data_t* in, out_data_t** out, size_t n_samples, 
	size_t n_channels, size_t in_datasize, [[maybe_unused]] size_t out_datasize, bool out_add)
{
	auto make_out = out_add ? make_out_add<out_data_t> : make_out_move<out_data_t>;
	if constexpr (is_integral_v<out_data_t>)
	{
		auto shift = (static_cast<ptrdiff_t>(out_datasize) - static_cast<ptrdiff_t>(in_datasize)) * 8;
		for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
			fixed_point_converter_interleaver_deinterleaver(
				in + channel_index,
				out[channel_index],
				n_samples,
				n_channels,
				1,
				shift,
				out_datasize,
				make_out);
	}
	else
	{
		auto gain_scaler = static_cast<int64_t>(max_datatype_value<in_data_t>(in_datasize));
		for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
			float_point_converter_interleaver_deinterleaver(
				in + channel_index,
				out[channel_index],
				n_samples,
				n_channels,
				1,
				gain_scaler,
				make_out);
	}
}

template void to_fixed8<>(float*  , int8_t*, size_t, size_t, size_t);
template void to_fixed8<>(double* , int8_t*, size_t, size_t, size_t);
template void to_fixed8<>(int8_t* , int8_t*, size_t, size_t, size_t);
template void to_fixed8<>(int16_t*, int8_t*, size_t, size_t, size_t);
template void to_fixed8<>(int32_t*, int8_t*, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed8(in_data_t* in, int8_t* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int8_t), false);
}

template void to_fixed8<>(float**  , int8_t**, size_t, size_t, size_t);
template void to_fixed8<>(double** , int8_t**, size_t, size_t, size_t);
template void to_fixed8<>(int8_t** , int8_t**, size_t, size_t, size_t);
template void to_fixed8<>(int16_t**, int8_t**, size_t, size_t, size_t);
template void to_fixed8<>(int32_t**, int8_t**, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed8(in_data_t** in, int8_t** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int8_t), false);
}

template void to_fixed8<>(float**  , int8_t*, size_t, size_t, size_t);
template void to_fixed8<>(double** , int8_t*, size_t, size_t, size_t);
template void to_fixed8<>(int8_t** , int8_t*, size_t, size_t, size_t);
template void to_fixed8<>(int16_t**, int8_t*, size_t, size_t, size_t);
template void to_fixed8<>(int32_t**, int8_t*, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed8(in_data_t** in, int8_t* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int8_t), false);
}

template void to_fixed8<>(float*  , int8_t**, size_t, size_t, size_t);
template void to_fixed8<>(double* , int8_t**, size_t, size_t, size_t);
template void to_fixed8<>(int8_t* , int8_t**, size_t, size_t, size_t);
template void to_fixed8<>(int16_t*, int8_t**, size_t, size_t, size_t);
template void to_fixed8<>(int32_t*, int8_t**, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed8(in_data_t* in, int8_t** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int8_t), false);
}

template void to_fixed16<>(float*  , int16_t*, size_t, size_t, size_t);
template void to_fixed16<>(double* , int16_t*, size_t, size_t, size_t);
template void to_fixed16<>(int8_t* , int16_t*, size_t, size_t, size_t);
template void to_fixed16<>(int16_t*, int16_t*, size_t, size_t, size_t);
template void to_fixed16<>(int32_t*, int16_t*, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed16(in_data_t* in, int16_t* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int16_t), false);
}

template void to_fixed16<>(float**  , int16_t**, size_t, size_t, size_t);
template void to_fixed16<>(double** , int16_t**, size_t, size_t, size_t);
template void to_fixed16<>(int8_t** , int16_t**, size_t, size_t, size_t);
template void to_fixed16<>(int16_t**, int16_t**, size_t, size_t, size_t);
template void to_fixed16<>(int32_t**, int16_t**, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed16(in_data_t** in, int16_t** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int16_t), false);
}

template void to_fixed16<>(float**  , int16_t*, size_t, size_t, size_t);
template void to_fixed16<>(double** , int16_t*, size_t, size_t, size_t);
template void to_fixed16<>(int8_t** , int16_t*, size_t, size_t, size_t);
template void to_fixed16<>(int16_t**, int16_t*, size_t, size_t, size_t);
template void to_fixed16<>(int32_t**, int16_t*, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed16(in_data_t** in, int16_t* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int16_t), false);
}

template void to_fixed16<>(float*  , int16_t**, size_t, size_t, size_t);
template void to_fixed16<>(double* , int16_t**, size_t, size_t, size_t);
template void to_fixed16<>(int8_t* , int16_t**, size_t, size_t, size_t);
template void to_fixed16<>(int16_t*, int16_t**, size_t, size_t, size_t);
template void to_fixed16<>(int32_t*, int16_t**, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed16(in_data_t* in, int16_t** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int16_t), false);
}

template void to_fixed24<>(float*  , int32_t*, size_t, size_t, size_t);
template void to_fixed24<>(double* , int32_t*, size_t, size_t, size_t);
template void to_fixed24<>(int8_t* , int32_t*, size_t, size_t, size_t);
template void to_fixed24<>(int16_t*, int32_t*, size_t, size_t, size_t);
template void to_fixed24<>(int32_t*, int32_t*, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed24(in_data_t* in, int32_t* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof_fixed24, false);
}

template void to_fixed24<>(float**  , int32_t**, size_t, size_t, size_t);
template void to_fixed24<>(double** , int32_t**, size_t, size_t, size_t);
template void to_fixed24<>(int8_t** , int32_t**, size_t, size_t, size_t);
template void to_fixed24<>(int16_t**, int32_t**, size_t, size_t, size_t);
template void to_fixed24<>(int32_t**, int32_t**, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed24(in_data_t** in, int32_t** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof_fixed24, false);
}

template void to_fixed24<>(float**  , int32_t*, size_t, size_t, size_t);
template void to_fixed24<>(double** , int32_t*, size_t, size_t, size_t);
template void to_fixed24<>(int8_t** , int32_t*, size_t, size_t, size_t);
template void to_fixed24<>(int16_t**, int32_t*, size_t, size_t, size_t);
template void to_fixed24<>(int32_t**, int32_t*, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed24(in_data_t** in, int32_t* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof_fixed24, false);
}

template void to_fixed24<>(float*  , int32_t**, size_t, size_t, size_t);
template void to_fixed24<>(double* , int32_t**, size_t, size_t, size_t);
template void to_fixed24<>(int8_t* , int32_t**, size_t, size_t, size_t);
template void to_fixed24<>(int16_t*, int32_t**, size_t, size_t, size_t);
template void to_fixed24<>(int32_t*, int32_t**, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed24(in_data_t* in, int32_t** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof_fixed24, false);
}

template void to_fixed32<>(float*  , int32_t*, size_t, size_t, size_t);
template void to_fixed32<>(double* , int32_t*, size_t, size_t, size_t);
template void to_fixed32<>(int8_t* , int32_t*, size_t, size_t, size_t);
template void to_fixed32<>(int16_t*, int32_t*, size_t, size_t, size_t);
template void to_fixed32<>(int32_t*, int32_t*, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed32(in_data_t* in, int32_t* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int32_t), false);
}

template void to_fixed32<>(float**  , int32_t**, size_t, size_t, size_t);
template void to_fixed32<>(double** , int32_t**, size_t, size_t, size_t);
template void to_fixed32<>(int8_t** , int32_t**, size_t, size_t, size_t);
template void to_fixed32<>(int16_t**, int32_t**, size_t, size_t, size_t);
template void to_fixed32<>(int32_t**, int32_t**, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed32(in_data_t** in, int32_t** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int32_t), false);
}

template void to_fixed32<>(float**  , int32_t*, size_t, size_t, size_t);
template void to_fixed32<>(double** , int32_t*, size_t, size_t, size_t);
template void to_fixed32<>(int8_t** , int32_t*, size_t, size_t, size_t);
template void to_fixed32<>(int16_t**, int32_t*, size_t, size_t, size_t);
template void to_fixed32<>(int32_t**, int32_t*, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed32(in_data_t** in, int32_t* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int32_t), false);
}

template void to_fixed32<>(float*  , int32_t**, size_t, size_t, size_t);
template void to_fixed32<>(double* , int32_t**, size_t, size_t, size_t);
template void to_fixed32<>(int8_t* , int32_t**, size_t, size_t, size_t);
template void to_fixed32<>(int16_t*, int32_t**, size_t, size_t, size_t);
template void to_fixed32<>(int32_t*, int32_t**, size_t, size_t, size_t);
template <typename in_data_t>
void to_fixed32(in_data_t* in, int32_t** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, 
		in_datasize, sizeof(int32_t), false);
}

template void to_float32<>(float*  , float*, size_t, size_t, size_t);
template void to_float32<>(double* , float*, size_t, size_t, size_t);
template void to_float32<>(int8_t* , float*, size_t, size_t, size_t);
template void to_float32<>(int16_t*, float*, size_t, size_t, size_t);
template void to_float32<>(int32_t*, float*, size_t, size_t, size_t);
template <typename in_data_t>
void to_float32(in_data_t* in, float* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, in_datasize, NULL, false);
}

template void to_float32<>(float**  , float*, size_t, size_t, size_t);
template void to_float32<>(double** , float*, size_t, size_t, size_t);
template void to_float32<>(int8_t** , float*, size_t, size_t, size_t);
template void to_float32<>(int16_t**, float*, size_t, size_t, size_t);
template void to_float32<>(int32_t**, float*, size_t, size_t, size_t);
template <typename in_data_t>
void to_float32(in_data_t** in, float* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, in_datasize, NULL, false);
}

template void to_float32<>(float*  , float**, size_t, size_t, size_t);
template void to_float32<>(double* , float**, size_t, size_t, size_t);
template void to_float32<>(int8_t* , float**, size_t, size_t, size_t);
template void to_float32<>(int16_t*, float**, size_t, size_t, size_t);
template void to_float32<>(int32_t*, float**, size_t, size_t, size_t);
template <typename in_data_t>
void to_float32(in_data_t* in, float** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, in_datasize, NULL, false);
}

template void to_float32<>(float**  , float**, size_t, size_t, size_t);
template void to_float32<>(double** , float**, size_t, size_t, size_t);
template void to_float32<>(int8_t** , float**, size_t, size_t, size_t);
template void to_float32<>(int16_t**, float**, size_t, size_t, size_t);
template void to_float32<>(int32_t**, float**, size_t, size_t, size_t);
template <typename in_data_t>
void to_float32(in_data_t** in, float** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, in_datasize, NULL, false);
}

template void to_float64<>(float*  , double*, size_t, size_t, size_t);
template void to_float64<>(double* , double*, size_t, size_t, size_t);
template void to_float64<>(int8_t* , double*, size_t, size_t, size_t);
template void to_float64<>(int16_t*, double*, size_t, size_t, size_t);
template void to_float64<>(int32_t*, double*, size_t, size_t, size_t);
template <typename in_data_t>
void to_float64(in_data_t* in, double* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, in_datasize, NULL, false);
}

template void to_float64<>(float**  , double**, size_t, size_t, size_t);
template void to_float64<>(double** , double**, size_t, size_t, size_t);
template void to_float64<>(int8_t** , double**, size_t, size_t, size_t);
template void to_float64<>(int16_t**, double**, size_t, size_t, size_t);
template void to_float64<>(int32_t**, double**, size_t, size_t, size_t);
template <typename in_data_t>
void to_float64(in_data_t** in, double** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, in_datasize, NULL, false);
}

template void to_float64<>(float*  , double**, size_t, size_t, size_t);
template void to_float64<>(double* , double**, size_t, size_t, size_t);
template void to_float64<>(int8_t* , double**, size_t, size_t, size_t);
template void to_float64<>(int16_t*, double**, size_t, size_t, size_t);
template void to_float64<>(int32_t*, double**, size_t, size_t, size_t);
template <typename in_data_t>
void to_float64(in_data_t* in, double** out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, in_datasize, NULL, false);
}

template void to_float64<>(float**  , double*, size_t, size_t, size_t);
template void to_float64<>(double** , double*, size_t, size_t, size_t);
template void to_float64<>(int8_t** , double*, size_t, size_t, size_t);
template void to_float64<>(int16_t**, double*, size_t, size_t, size_t);
template void to_float64<>(int32_t**, double*, size_t, size_t, size_t);
template <typename in_data_t>
void to_float64(in_data_t** in, double* out, size_t n_samples, size_t n_channels, size_t in_datasize)
{
	converter_interleaver_deinterleaver(in, out, n_samples, n_channels, in_datasize, NULL, false);
}

template <typename data_t>
void to_mono(vector<data_t*> &channels, data_t* __restrict out, size_t n_samples, ptrdiff_t in_step)
{
	for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
	{
		double mixed = 0;
		for (auto channel = channels.begin(); channel < channels.end(); channel++)
		{
			mixed += **channel;
			(*channel) += in_step;
		}
		mixed /= channels.size();
		*out++ = static_cast<data_t>(mixed);
	}
}

template void to_mono<>(float*  , float*  , size_t, size_t);
template void to_mono<>(double* , double* , size_t, size_t);
template void to_mono<>(int8_t* , int8_t* , size_t, size_t);
template void to_mono<>(int16_t*, int16_t*, size_t, size_t);
template void to_mono<>(int32_t*, int32_t*, size_t, size_t);
template <typename data_t>
void to_mono(data_t* in, data_t* out, size_t n_samples, size_t n_channels)
{
	vector<data_t*> channels;
	for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
		channels.push_back(in + channel_index);
	to_mono(channels, out, n_samples, n_channels);
}

template void to_mono<>(float**  , float*  , size_t, size_t);
template void to_mono<>(double** , double* , size_t, size_t);
template void to_mono<>(int8_t** , int8_t* , size_t, size_t);
template void to_mono<>(int16_t**, int16_t*, size_t, size_t);
template void to_mono<>(int32_t**, int32_t*, size_t, size_t);
template <typename data_t>
void to_mono(data_t** in, data_t* out, size_t n_samples, size_t n_channels)
{
	vector<data_t*> channels;
	for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
		channels.push_back(in[channel_index]);
	to_mono(channels, out, n_samples, 1);
}

template void data_reverse<>(float*  , size_t, size_t);
template void data_reverse<>(double* , size_t, size_t);
template void data_reverse<>(int8_t* , size_t, size_t);
template void data_reverse<>(int16_t*, size_t, size_t);
template void data_reverse<>(int32_t*, size_t, size_t);
template <typename data_t>
void data_reverse(data_t* in_out, size_t n_samples, size_t n_channels)
{
    data_t sample_under_processing;
    auto first_sample = in_out;
    auto last_sample = in_out + (n_samples - 1) * n_channels;
    n_samples >>= 1;
    for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
    {
        for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
        {
            sample_under_processing = *first_sample;
            *first_sample++ = *last_sample;
            *last_sample++ = sample_under_processing;
        }
        last_sample -= n_channels;
        last_sample -= n_channels;
    }
}

template void data_reverse<>(float*  , float*  , size_t, size_t);
template void data_reverse<>(double* , double* , size_t, size_t);
template void data_reverse<>(int8_t* , int8_t* , size_t, size_t);
template void data_reverse<>(int16_t*, int16_t*, size_t, size_t);
template void data_reverse<>(int32_t*, int32_t*, size_t, size_t);
template <typename data_t>
void data_reverse(data_t* in, data_t* out, size_t n_samples, size_t n_channels)
{
    auto firstSample = in;
    auto lastSample = out + (n_samples - 1) * n_channels;
    for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
    {
        for (size_t channel_index = 0; channel_index < n_channels; channel_index++)
            *lastSample++ = *firstSample++;

        lastSample -= n_channels;
        lastSample -= n_channels;
    }
}
}
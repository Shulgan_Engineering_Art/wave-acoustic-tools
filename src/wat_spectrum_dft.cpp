﻿/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools.
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#include <cassert>
#if defined(WAT_OPENMP_ENABLE)
#include <omp.h>
#endif
#include "wat_math.hpp"
#include "wat_spectrum_dft.hpp"

namespace sea_wat
{

template class spectrum_dft_t<float>;
template class spectrum_dft_t<double>;
template class spectrum_dft_t<int8_t>;
template class spectrum_dft_t<int16_t>;
template class spectrum_dft_t<int32_t>;

template <typename data_t>
spectrum_dft_t<data_t>::spectrum_dft_t(const spectrum_dft_set_t& set)
{
    this->init(set);
}

template <typename data_t>
spectrum_dft_t<data_t>::spectrum_dft_t()
{
    this->init(spectrum_dft_default);
}

template <typename data_t>
spectrum_dft_t<data_t>::~spectrum_dft_t()
{
    this->dealloc();
}

template <typename data_t>
void spectrum_dft_t<data_t>::init(const spectrum_dft_set_t& set)
{
    assert(set.band_begin < set.band_end);
    assert(set.overlap_n_samples < set.window_n_samples);
    
    this->dealloc();
    this->n_channels = set.n_channels;
    this->n_bands = set.n_bands;

    this->band_begin_rad = static_cast<float>(pi_x2) * set.band_begin;
    this->band_step_rad = static_cast<float>(pi_x2) * (set.band_end - set.band_begin) / this->n_bands;

    auto window_tail_n_samples = set.window_n_samples - set.overlap_n_samples;
    auto storage_depth = set.overlap_n_samples / window_tail_n_samples;
    if (set.overlap_n_samples % window_tail_n_samples)
        storage_depth += 1;

    // Window parts markup (parts = storage_depth + 1)
    this->window_parts_n_samples.push_back(set.window_n_samples - set.overlap_n_samples);
    for (size_t part_index = 1; part_index < storage_depth; part_index++)
        this->window_parts_n_samples.push_back(
            this->window_parts_n_samples.back() + set.window_n_samples - set.overlap_n_samples);
    this->window_parts_n_samples.push_back(set.window_n_samples);

    // Spectrum parts allocate
    for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
    {
        this->spectrum_parts.push_back({{new complex<float>[this->n_bands]}});
        for (size_t part_index = 1; part_index < storage_depth + 1; part_index++)
            this->spectrum_parts.back().push_back(new complex<float>[this->n_bands]);
    }



    {
        // Zeroing current window spectrum
        for (auto part : this->spectrum_parts.back())
            for (size_t sample_index = 0; sample_index < this->n_bands; sample_index++)
                *part++ = complex<float>(0.f, 0.f);

        this->sample_phase_index = 0;
        this->samples_cnt = 0;
        this->window_part_index = 0;
    }
}

template <typename data_t>
size_t spectrum_dft_t<data_t>::process(data_t* in, size_t n_samples)
{
    vector<data_t*> in_vec;
    for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
		in_vec.push_back(in + channel_index);
    return this->process(in_vec, n_samples, this->n_channels);
}

template <typename data_t>
size_t spectrum_dft_t<data_t>::process(data_t** in, size_t n_samples)
{
    vector<data_t*> in_vec;
    for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
		in_vec.push_back(in[channel_index]);
    return this->process(in_vec, n_samples, 1);
}

template <typename data_t>
inline void spectrum_dft_t<data_t>::n_channels_dft(
    const vector<data_t*>& in, size_t n_samples, ptrdiff_t in_step)
{
#if defined(WAT_OPENMP_ENABLE)
#pragma omp parallel for
#endif
    for (auto channel : this->spectrum_parts)
    {
        auto in_for_channel = in.begin();
        auto band_value = channel[this->window_part_index];
        auto band = this->band_begin_rad;
        for (size_t band_index = 0; band_index < this->n_bands; band_index++)
        {
            *band_value++ += dft(*in_for_channel, n_samples, band, in_step, this->sample_phase_index);
            band += this->band_step_rad;
        }
        in_for_channel++;
    }
    this->sample_phase_index += n_samples; // нужно обнулять, перенести внутрь dft и там обнулять
}

// https://habr.com/ru/post/430536/
template <typename data_t>
size_t spectrum_dft_t<data_t>::process(const vector<data_t*> &in, size_t n_samples, ptrdiff_t in_step)
{
    size_t current_part_n_samples = n_samples;
    auto in_local = in;
    
    this->samples_cnt += n_samples;
    if (this->samples_cnt > this->window_parts_n_samples[this->window_part_index])
    {
        // next part
        auto next_part_n_samples = this->samples_cnt % this->window_parts_n_samples[this->window_part_index];       
        current_part_n_samples = n_samples - next_part_n_samples;
        this->n_channels_dft(in_local, current_part_n_samples, in_step);
        for (auto in_for_channel = in_local.begin(); in_for_channel < in_local.end(); in_for_channel++)
            *in_for_channel += current_part_n_samples;
        current_part_n_samples = next_part_n_samples;
        this->window_part_index++;

        if (this->window_part_index >= this->window_parts_n_samples.size())
        {
            // End window

            // суммировать все части спектра
            // применить функцию окна
            // суммировать с предыдущими накладками из overlap_history
            // удалить самую первую в overlap_history
            // отправить в spectrum_history
            // суммировать соответствующие части для overlap_history
            // применить к ним оконную функцию
            // сохранить результаты в overlap_history как последнюю запись
            // суммировать соответствующие части для spectrum_history
            // применить к ним оконную функцию
            // суммировать соответственно с записями из spectrum_history
            // переместить первую запись из spectrum_history в spectrums (результирующие спектры)
            // обнулить счетчики и spectrum_parts
        }
    }

    this->n_channels_dft(in_local, current_part_n_samples, in_step);
    return n_samples;
}

template <typename data_t>
error_code spectrum_dft_t<data_t>::get_amplitudes(vector<vector<float*>>* amplitudes) // одинаковый код объединить в одну функцию
{
    if (!amplitudes)
        return make_error_code(errc::invalid_argument);

    for (auto window : this->spectrums_history)
        amplitudes->push_back({});

#if defined(WAT_OPENMP_ENABLE)
#pragma omp parallel for
#endif        
    for (size_t window_index = 0;  window_index < this->spectrums_history.size(); window_index++)
        for (size_t channel_index = 0; channel_index < this->spectrums_history[window_index].size(); channel_index++)
        {
            (*amplitudes)[window_index].push_back(new float[this->n_bands]); // нужно ли выделять память внутри?
            auto band_amplitude = (*amplitudes)[window_index].back();
            auto band = this->spectrums_history[window_index][channel_index];
            for (size_t band_index = 0; band_index < this->n_bands; band_index++)
                *band_amplitude++ = abs(*band++);
        }
    return error_code{};
}

template <typename data_t>
error_code spectrum_dft_t<data_t>::get_phases(vector<vector<float*>>* phases)
{   
    if (!phases)
        return make_error_code(errc::invalid_argument);

    for (auto window : this->spectrums_history)
        phases->push_back({});

#if defined(WAT_OPENMP_ENABLE)
#pragma omp parallel for
#endif
    for (size_t window_index = 0; window_index < this->spectrums_history.size(); window_index++)
        for (size_t channel_index = 0; channel_index < this->spectrums_history[window_index].size(); channel_index++)
        {
            (*phases)[window_index].push_back(new float[this->n_bands]);
            auto band_phase = (*phases)[window_index].back();
            auto band = this->spectrums_history[window_index][channel_index];
            for (size_t band_index = 0; band_index < this->n_bands; band_index++)
                *band_phase++ = arg(*band++);
        }
    return error_code{};
}

template <typename data_t>
vector<vector<complex<float>*>>* spectrum_dft_t<data_t>::get_spectrums()
{
    return &this->spectrums_history;
}

template <typename data_t>
void spectrum_dft_t<data_t>::clear_history()
{

}

template <typename data_t>
void spectrum_dft_t<data_t>::dealloc()
{
    for (auto window : this->spectrums_history)
        for (auto spectrum : window)
            delete[] spectrum;
    this->spectrums_history.clear();
    this->window_parts_n_samples.clear();
}

const spectrum_dft_set_t spectrum_dft_default = {1, 1000, 0.f, 0.5f, 0, 1000, 1};
}
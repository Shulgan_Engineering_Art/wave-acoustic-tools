﻿/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#include <cassert>
#if defined(WAT_OPENMP_ENABLE)
#include <omp.h>
#endif
#include "wat_primitives.hpp"
#include "wat_math.hpp"
#include "wat_fir.hpp"

namespace sea_wat
{

template class fir_t<float>;
template class fir_t<double>;
template class fir_t<int8_t>;
template class fir_t<int16_t>;
template class fir_t<int32_t>;

template <typename data_t>
fir_t<data_t>::fir_t(const fir_set_t& set)
{
	this->filter_model = nullptr;
	this->init(set);
}

template <typename data_t>
fir_t<data_t>::fir_t(const fir_set_Hz_t& set)
{
	assert(set.band_begin_Hz < set.samplerate_Hz);
	assert(set.band_end_Hz < set.samplerate_Hz);
	this->filter_model = nullptr;
	this->init(fir_set_t 
	{
		set.fir_type,
		set.fir_response_type,
		set.n_channels,
		static_cast<float>(set.band_begin_Hz) / set.samplerate_Hz,
		static_cast<float>(set.band_end_Hz) / set.samplerate_Hz,
		static_cast<float>(set.transition_width_Hz) / set.samplerate_Hz,
		set.stop_band_gain_dB,
		set.pass_band_gain_dB,
		set.pass_band_ripple_dB
	});
}

template <typename data_t>
fir_t<data_t>::fir_t(const fir_set_fixed_len_t& set)
{
	this->filter_model = nullptr;
	this->init(set);
}

template <typename data_t>
fir_t<data_t>::fir_t(const fir_set_fixed_len_Hz_t& set)
{
	assert(set.band_begin_Hz < set.samplerate_Hz);
	assert(set.band_end_Hz < set.samplerate_Hz);
	this->filter_model = nullptr;
	this->init(fir_set_fixed_len_t 
	{
		set.fir_type,
		set.fir_response_type,
		set.n_channels,
		static_cast<float>(set.band_begin_Hz) / set.samplerate_Hz,
		static_cast<float>(set.band_end_Hz) / set.samplerate_Hz,
		set.fir_length,
		set.stop_band_gain_dB,
		set.pass_band_gain_dB,
	});
}

template <typename data_t>
fir_t<data_t>::fir_t(const fir_set_external_t& set)
{
	this->filter_model = nullptr;
	this->init(set);
}

template <typename data_t>
fir_t<data_t>::fir_t()
{
	this->filter_model = nullptr;
	this->init(fir_default);
}

template <typename data_t>
fir_t<data_t>::~fir_t()
{
	this->dealloc();
}

template <typename data_t>
template <typename set_t>
void fir_t<data_t>::init(const set_t& set)
{	
	static_assert(is_same_v<decay_t<set_t>, fir_set_t> || 
		is_same_v<decay_t<set_t>, fir_set_fixed_len_t> || is_same_v<decay_t<set_t>, fir_set_external_t>);

	if constexpr (is_same_v<decay_t<set_t>, fir_set_external_t>)
	{
		this->dealloc();
		this->n_channels = set.n_channels;
		this->is_external_coeffs = true;
		this->length = set.fir_length;
	}
	else
	{
		assert(set.stop_band_gain_dB < set.pass_band_gain_dB);
		assert(set.band_begin < set.band_end);
		assert(set.band_begin >= 0);
		assert(set.band_begin < 0.5);
		assert(set.band_end >= 0);
		assert(set.band_end < 0.5);

		this->dealloc();
		this->n_channels = set.n_channels;
		this->is_external_coeffs = false;
		auto transition_width = 0.f;
		auto band_pass_ripple_dB = 0.f;
		if constexpr (is_same_v<decay_t<set_t>, fir_set_t>)
		{
			transition_width = set.transition_width;
			band_pass_ripple_dB = set.pass_band_ripple_dB;
			this->length = 0;
		}
		else
		{
			this->length = set.fir_length;
		}
		if constexpr (is_integral_v<data_t>)
		{
			this->template make_response<int32_t>(set.band_begin, set.band_end,
				transition_width, set.stop_band_gain_dB, set.pass_band_gain_dB, band_pass_ripple_dB,
				set.fir_type, set.fir_response_type);
			this->out_gain_shift = sizeof(int32_t) * 8 - 1;
		}
		else
		{
			this->template make_response<data_t>(set.band_begin, set.band_end,
				transition_width, set.stop_band_gain_dB, set.pass_band_gain_dB, band_pass_ripple_dB,
				set.fir_type, set.fir_response_type);
			this->out_gain_shift = 0;
		}
		this->start_delay_size = this->length / 2;
	}
	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		this->prev_samples.push_back(new data_t[this->length]);
		auto sample = static_cast<data_t*>(this->prev_samples.back());
		for (size_t sample_index = 0; sample_index < this->length; sample_index++)
			*sample++ = static_cast<data_t>(0);
	}
}

template <typename data_t>
template <typename coeff_t>
void fir_t<data_t>::make_response(float band_begin, float band_end, float transition_width,
	float stop_band_gain_dB, float pass_band_gain_dB, float pass_band_ripple_dB, 
	fir_type_t fir_type, fir_response_t response_type)
{
	if (transition_width != 0.f)
		this->length = fir_filter_length(transition_width, stop_band_gain_dB, pass_band_gain_dB, pass_band_ripple_dB);
	
	this->filter_model = new coeff_t[this->length];
	auto coeffs = static_cast<coeff_t*>(this->filter_model);
	float band_gain_dB;
	if (fir_type == band_pass)
	{
		sinc(coeffs, this->length, band_begin, band_end);
		band_gain_dB = pass_band_gain_dB;
	}
	else if (fir_type == band_stop)
	{
		sinc(coeffs, this->length, band_end, band_begin);
		band_gain_dB = stop_band_gain_dB;
	}
	else
		return;
	switch(response_type)
	{
	case rectangular :
		break;
	case Kaiser_win :
		fit_to_Kaiser_win(coeffs, this->length, stop_band_gain_dB);
		break;
	case Hann_win :
		fit_to_Hann_win(coeffs, this->length);
		break;
	case Bartlett_win :
		fit_to_Bartlett_win(coeffs, this->length);
		break;
	case Blackman_win :
		fit_to_Blackman_win(coeffs, this->length);
		break;
	default :
		break;
	}
	this->gain_adj(coeffs, static_cast<float>(band_begin +
		(band_end - band_begin) / 2), band_gain_dB);
}

template void fir_t<float>::  set_external_coeffs<>(float*  );
template void fir_t<double>:: set_external_coeffs<>(double* );
template void fir_t<int8_t>:: set_external_coeffs<>(int32_t*);
template void fir_t<int16_t>::set_external_coeffs<>(int32_t*);
template void fir_t<int32_t>::set_external_coeffs<>(int32_t*);
template <typename data_t>
template <typename coeff_t>
void fir_t<data_t>::set_external_coeffs(coeff_t* coeffs)
{
	if constexpr (is_integral_v<data_t>)
		static_assert(sizeof(coeff_t) == sizeof(int32_t), 
			"coefficient type - int32_t only for fixed point data types");
	else
		static_assert(sizeof(coeff_t) == sizeof(data_t), 
			"data type and coefficient type should be the same for float point types");

	assert(this->is_external_coeffs);
    this->filter_model = coeffs;
}

template <typename data_t>
void* fir_t<data_t>::get_model()
{
	return this->filter_model;
}

template <typename data_t>
size_t fir_t<data_t>::get_length()
{
	return this->length;
}

template <typename data_t>
template <typename coeff_t>
void fir_t<data_t>::gain_adj(coeff_t* coeffs, float band, float gain_dB)
{
	auto scaler = abs(dft(coeffs, this->length, static_cast<float>(pi_x2) * band, 1))
		/ max_datatype_value<coeff_t>() / dB_to_linear<float>(gain_dB);
    for (size_t coeff_index = 0; coeff_index < this->length; coeff_index++)
        coeffs[coeff_index] = static_cast<coeff_t>(coeffs[coeff_index] / scaler);
}

template <typename data_t>
size_t fir_t<data_t>::process(data_t* in, data_t* out, size_t n_samples)
{
	vector<data_t*> in_vec, out_vec;
	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		in_vec.push_back(in + channel_index);
		out_vec.push_back(out + channel_index);
	}
	if constexpr (is_integral_v<data_t>)
		return this->process<int32_t>(in_vec, out_vec, n_samples, 
			this->n_channels, this->n_channels);
	else
	{
		if constexpr (is_same_v<data_t, float>)
			return this->process<float>(in_vec, out_vec, n_samples, 
				this->n_channels, this->n_channels);
		else if constexpr (is_same_v<data_t, double>)
			return this->process<double>(in_vec, out_vec, n_samples, 
				this->n_channels, this->n_channels);
	}
	
}

template <typename data_t>
size_t fir_t<data_t>::process(data_t** in, data_t** out, size_t n_samples)
{
	vector<data_t*> in_vec, out_vec;
	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		in_vec.push_back(in[channel_index]);
		out_vec.push_back(out[channel_index]);
	}
	if constexpr (is_integral_v<data_t>)
		return this->process<int32_t>(in_vec, out_vec, n_samples, 1, 1);
	else
	{
		if constexpr (is_same_v<data_t, float>)
			return this->process<float>(in_vec, out_vec, n_samples, 1, 1);
		else if constexpr (is_same_v<data_t, double>)
			return this->process<double>(in_vec, out_vec, n_samples, 1, 1);
	}
	
}

template size_t fir_t<float>  ::process<float>  (const vector<float*>&,   const vector<float*>&, 
	size_t, ptrdiff_t, ptrdiff_t);
template size_t fir_t<double> ::process<double> (const vector<double*>&,  const vector<double*>&, 
	size_t, ptrdiff_t, ptrdiff_t);
template size_t fir_t<int8_t> ::process<int32_t>(const vector<int8_t*>&,  const vector<int8_t*>&, 
	size_t, ptrdiff_t, ptrdiff_t);
template size_t fir_t<int16_t>::process<int32_t>(const vector<int16_t*>&, const vector<int16_t*>&, 
	size_t, ptrdiff_t, ptrdiff_t);
template size_t fir_t<int32_t>::process<int32_t>(const vector<int32_t*>&, const vector<int32_t*>&, 
	size_t, ptrdiff_t, ptrdiff_t);
template <typename data_t>
template <typename coeff_t>
size_t fir_t<data_t>::process(const vector<data_t*>& in, const vector<data_t*>& out, size_t n_samples,
	ptrdiff_t in_step, ptrdiff_t out_step)
{
	assert(in.size() == this->n_channels);
	assert(out.size() == this->n_channels);

	auto n_samples_to_save = this->length < n_samples ? this->length : n_samples;
	size_t n_prev_samples_to_keep = 0;
	if (n_samples_to_save < this->length)
		n_prev_samples_to_keep = this->length - n_samples_to_save;

	// тут обрабатывается первый полностью нулевой буффер
	for (size_t channel_index = 0; channel_index < this->n_channels; channel_index++)
	{
		auto out_sample = out[channel_index];
		for (size_t out_sample_index = 0; out_sample_index < n_samples; out_sample_index++)
		{
			size_t n_prev_samples = 0;
			if (this->length > out_sample_index)
				n_prev_samples = this->length - out_sample_index;
			size_t n_current_samples = out_sample_index;
			
			auto sample = static_cast<data_t*>(this->prev_samples[channel_index]) + 
				this->length - 1 - out_sample_index;
			auto coeff = static_cast<coeff_t*>(this->filter_model);
			auto acc = static_cast<double>(0.);
			for (size_t sample_index = 0; sample_index < n_prev_samples; sample_index++)
			{
				acc += *sample-- * *coeff++;
			}
			sample = in[channel_index];
			if (out_sample_index > this->length)
			{
				sample += out_sample_index - this->length;
				n_current_samples = this->length;
			}
			for (size_t sample_index = 0; sample_index < n_current_samples; sample_index++)
			{
				acc += *sample * *coeff++;
				sample += in_step;
			}
			*out_sample = static_cast<data_t>(acc);
			out_sample += out_step;
		}
	}

	auto prev_samples_for_channel = this->prev_samples.begin();
#if defined(WAT_OPENMP_ENABLE)
//#pragma omp parallel for
#endif
	for (auto in_for_channel = in.begin(); in_for_channel < in.end(); in_for_channel++)
	{
		// keep the newest samples and move them to the tail
		auto save_out = static_cast<data_t*>(*prev_samples_for_channel) + this->length - 1;
		if (n_prev_samples_to_keep)
		{
			auto save_in = static_cast<data_t*>(*prev_samples_for_channel) + 
				n_prev_samples_to_keep - 1;
			for (size_t sample_index = 0; sample_index < n_prev_samples_to_keep; sample_index++)
				*save_out-- = *save_in--;
		}
		// save the tail of samples to the local memory
		auto save_in = &(*in_for_channel)[(n_samples - n_samples_to_save) * in_step];
		for (size_t sample_index = 0; sample_index < n_samples_to_save; sample_index++)
		{
			*save_out-- = *save_in;
			save_in += in_step;
		}
		prev_samples_for_channel++;
	}

	return n_samples;
}

template <typename data_t>
void fir_t<data_t>::dealloc()
{
	if (this->filter_model && this->is_external_coeffs == false)
	{
		if constexpr (is_integral_v<data_t>)
			delete[] static_cast<int32_t*>(this->filter_model);
		else
		{
			if constexpr (sizeof(data_t) == sizeof(float))
				delete[] static_cast<float*>(this->filter_model);
			else if constexpr (sizeof(data_t) == sizeof(double))
				delete[] static_cast<double*>(this->filter_model);
		}
	}
	this->filter_model = nullptr;

	for (auto prev_buff = this->prev_samples.begin(); prev_buff < this->prev_samples.end(); prev_buff++)
		delete[] static_cast<data_t*>(*prev_buff);
	this->prev_samples.clear();
}

size_t fir_filter_length(float width, float stop_band_gain_dB, 
	float pass_band_gain_dB, float pass_band_ripple_dB)	// TODO: find correct name
{	
	// https://dsp.stackexchange.com/questions/31066/how-many-taps-does-an-fir-filter-need/31077#31077
	return static_cast<size_t>(log10f(1.f / 10 / 
		dB_to_linear<float>(stop_band_gain_dB - pass_band_gain_dB)
		/ dB_to_linear<float>(pass_band_ripple_dB + pass_band_gain_dB)) * 2 / 3 / width);
}

const fir_set_t fir_default = {band_pass, Kaiser_win, 1, 0.f, 0.2f, 0.05f, -70.f, 2.f, -70.f};
}
/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#include <cassert>
#include "wat_primitives.hpp"
#include "wat_math.hpp"

namespace sea_wat
{
const double pi = 3.14159265358979323846;
const double pi_x2 = pi * 2;
const int64_t phase_scaler = static_cast<int64_t>(10e8);
const int64_t pi_scaled = static_cast<int64_t>(pi * phase_scaler);
const int64_t pi_x2_scaled = static_cast<int64_t>(pi_x2 * phase_scaler);

template complex<float> dft<>(float*  , size_t, float, ptrdiff_t, size_t);
template complex<float> dft<>(double* , size_t, float, ptrdiff_t, size_t);
template complex<float> dft<>(int8_t* , size_t, float, ptrdiff_t, size_t);
template complex<float> dft<>(int16_t*, size_t, float, ptrdiff_t, size_t);
template complex<float> dft<>(int32_t*, size_t, float, ptrdiff_t, size_t);
template <typename data_t>
complex<float> dft(data_t* in, size_t n_samples, float band_rad, ptrdiff_t in_step, size_t start_phase_index)
{
	auto sum_re = 0.f, sum_im = 0.f;
	auto phase_step = static_cast<int64_t>(band_rad * phase_scaler);
	int64_t phase_scaled = phase_step * start_phase_index;
    for (size_t sample_index = start_phase_index; 
		sample_index < n_samples + start_phase_index; sample_index++)
    {
		phase_scaled = phase_scaled % pi_x2_scaled;
		auto phase = static_cast<float>(phase_scaled) / phase_scaler;
		sum_re += static_cast<float>(*in) * cosf(phase);
        sum_im += static_cast<float>(*in) * sinf(phase);
		in += in_step;
		phase_scaled += phase_step;
    }
    return complex<float>(sum_re, sum_im);
}

template complex<float> dft<>(complex<float>*, size_t, float, size_t);
template complex<float> dft<>(complex<double>*, size_t, float, size_t);
template complex<float> dft<>(complex<int8_t>*, size_t, float, size_t);
template complex<float> dft<>(complex<int16_t>*, size_t, float, size_t);
template complex<float> dft<>(complex<int32_t>*, size_t, float, size_t);
template <typename data_t>
complex<float> dft(complex<data_t>* in, size_t n_samples, float band_rad, size_t start_phase_index)
{
	auto sum_re = 0.f, sum_im = 0.f;
	auto phase_step = static_cast<int64_t>(band_rad * phase_scaler);
	int64_t phase_scaled = phase_step * start_phase_index;
	for (size_t sample_index = start_phase_index;
		sample_index < n_samples + start_phase_index; sample_index++)
	{
		phase_scaled = phase_scaled % pi_x2_scaled;
		auto phase = static_cast<float>(phase_scaled) / phase_scaler;
		sum_re += static_cast<float>(in->real()) * cosf(phase);
		sum_im += static_cast<float>(in->real()) * sinf(phase);
		in++;
		phase_scaled += phase_step;
	}
	return complex<float>(sum_re, sum_im);
}

/*template float   inv_dft<>(complex<float>*, size_t, size_t);
template double  inv_dft<>(complex<float>*, size_t, size_t);
template int8_t  inv_dft<>(complex<float>*, size_t, size_t);
template int16_t inv_dft<>(complex<float>*, size_t, size_t);
template int32_t inv_dft<>(complex<float>*, size_t, size_t);
template <typename data_t>
data_t inv_dft(complex<float>* in, size_t n_bands, size_t sample_index)
{
	// TODO 24 bit

}*/

double zeroeth_modified_Bessel_first_kind(double in)
{
	auto eps = 0.000001;
	auto result = 0.;
	auto term = 1.;
	auto m = 0.;

	// accumulate terms as long as they are significant
	while (term > eps * result)
	{
		result += term;
		++m;
		term *= (in * in) / (4 * m * m);
	}
	return result;
}

template void sinc<>(float*  , size_t, float, float, size_t);
template void sinc<>(double* , size_t, float, float, size_t);
template void sinc<>(int8_t* , size_t, float, float, size_t);
template void sinc<>(int16_t*, size_t, float, float, size_t);
template void sinc<>(int32_t*, size_t, float, float, size_t);
template <typename data_t>
void sinc(data_t* out, size_t length, float freq0, float freq1, size_t out_datasize)
{
	auto scaler = max_datatype_value<data_t>(out_datasize);
	auto half_size = length / 2;
	auto coeff = &out[half_size - 1];
	data_t* coeff_symmetric;
	if (length % 2)
	{
		for (double timestamp = 1.; timestamp < half_size + 1; timestamp += 1.)
			*coeff-- = static_cast<data_t>((sin(2. * pi * freq1 * timestamp) -
				sin(2. * pi * freq0 * timestamp)) / timestamp / pi * scaler);

		// central point
		if (freq0 < freq1)
			// band pass
			out[half_size] = static_cast<data_t>(2. * scaler * (freq1 - freq0));
		else
			// band stop
			out[half_size] = static_cast<data_t>(2. * scaler * (freq1 - freq0) + 1.);

		coeff_symmetric = &out[half_size + 1];
	}
	else
	{
		for (double timestamp = 0.5; timestamp < half_size + 0.5; timestamp += 1.)
			*coeff-- = static_cast<data_t>((sin(2. * pi * freq1 * timestamp) -
				sin(2. * pi * freq0 * timestamp)) / timestamp / pi * scaler);

		coeff_symmetric = &out[half_size];
	}
	coeff = &out[half_size - 1];
	for (size_t coeff_index = 0; coeff_index < half_size; coeff_index++)
		*coeff_symmetric++ = *coeff--;
}

// greatest common divisor
int64_t gcd(int64_t in0, int64_t in1)
{
	if (in1 == 0)
		return abs(in0);
	return gcd(in1, in0 % in1);
}

// least common multiple
int64_t lcm(int64_t in0, int64_t in1)
{
	return in0 / gcd(in0, in1) * in1;
}

template float   dBFS_to_linear<>(float, size_t);
template double  dBFS_to_linear<>(float, size_t);
template int8_t  dBFS_to_linear<>(float, size_t);
template int16_t dBFS_to_linear<>(float, size_t);
template int32_t dBFS_to_linear<>(float, size_t);
template <typename out_data_t>
out_data_t dBFS_to_linear(float dBFS, size_t out_datasize)
{
	if constexpr (is_integral_v<out_data_t>)
		assert(dBFS <= 0);
	
	return static_cast<out_data_t>(
		pow(10, dBFS / 20) * max_datatype_value<out_data_t>(out_datasize));
}

template float linear_to_dBFS<>(float  , size_t);
template float linear_to_dBFS<>(double , size_t);
template float linear_to_dBFS<>(int8_t , size_t);
template float linear_to_dBFS<>(int16_t, size_t);
template float linear_to_dBFS<>(int32_t, size_t);
template <typename in_data_t>
float linear_to_dBFS(in_data_t linear, size_t in_datasize)
{
	return static_cast<float>(20 * log10(
		static_cast<double>(linear) / max_datatype_value<in_data_t>(in_datasize)));
}

template float   dB_to_linear<>(float);
template double  dB_to_linear<>(float);
template int8_t  dB_to_linear<>(float);
template int16_t dB_to_linear<>(float);
template int32_t dB_to_linear<>(float);
template int64_t dB_to_linear<>(float);
template <typename out_data_t>
out_data_t dB_to_linear(float dB)
{
	return static_cast<out_data_t>(pow(10, dB / 20));
}

template float linear_to_dB<>(float);
template float linear_to_dB<>(double);
template float linear_to_dB<>(int8_t);
template float linear_to_dB<>(int16_t);
template float linear_to_dB<>(int32_t);
template float linear_to_dB<>(int64_t);
template <typename in_data_t>
float linear_to_dB(in_data_t linear)
{
	return static_cast<float>(20 * log10(static_cast<double>(linear)));
}

template void fit_to_Kaiser_win<>(float*, size_t, float);
template void fit_to_Kaiser_win<>(double*, size_t, float);
template void fit_to_Kaiser_win<>(int32_t*, size_t, float);
template <typename data_t>
void fit_to_Kaiser_win(data_t* coeffs, size_t length, float stop_band_dB)
{
	assert(stop_band_dB <= 0.f);
	// For more details see https://www.mathworks.com/help/signal/ref/kaiser.html
	auto Kaiser_shape = 0.;
	if (stop_band_dB < -50.f)
		Kaiser_shape = 0.1202 * (-stop_band_dB - 8.7);
	else if (stop_band_dB < 21.f)
		Kaiser_shape = 0.5842 * (pow((-stop_band_dB - 21.), 0.4)) + 0.07886 * (-stop_band_dB - 21.);
	
	auto coeff = coeffs;
	auto gain_scaler = zeroeth_modified_Bessel_first_kind(Kaiser_shape);
	for (size_t coeff_index = 0; coeff_index < length; coeff_index++)
	{
		*coeff = static_cast<data_t>(*coeff *
			zeroeth_modified_Bessel_first_kind(Kaiser_shape * 
			sqrt(1. - pow(2. * coeff_index / (length - 1) - 1., 2))) / gain_scaler);
		coeff++;
	}
}

template void fit_to_Hann_win<>(float*  , size_t);
template void fit_to_Hann_win<>(double* , size_t);
template void fit_to_Hann_win<>(int32_t*, size_t);
template <typename data_t>
void fit_to_Hann_win(data_t* coeffs, size_t length)
{
	auto filter_order = length - 1;
	auto max = 0.5 - 0.5 * cos(pi * length / filter_order);
	auto coeff = coeffs;
	for (size_t coeff_index = 0; coeff_index < length; coeff_index++)
		*coeff = static_cast<data_t>(*coeff *
		(0.5 - 0.5 * cos(pi_x2 * coeff_index / filter_order)) / max);
	coeff++;
}

template void fit_to_Bartlett_win<>(float*  , size_t);
template void fit_to_Bartlett_win<>(double* , size_t);
template void fit_to_Bartlett_win<>(int32_t*, size_t);
template <typename data_t>
void fit_to_Bartlett_win(data_t* coeffs, size_t length)
{
	auto A = (length - 1) / 2.;
	auto coeff = coeffs;
	for (size_t coeff_index = 0; coeff_index < length; coeff_index++)
	{
		*coeff = static_cast<data_t>(*coeff * (1 - abs(coeff_index / A - 1)));
		coeff++;
	}
}

template void fit_to_Blackman_win<>(float*  , size_t);
template void fit_to_Blackman_win<>(double* , size_t);
template void fit_to_Blackman_win<>(int32_t*, size_t);
template <typename data_t>
void fit_to_Blackman_win(data_t* coeffs, size_t length)
{
	auto filter_order = length - 1;
	auto coeff = coeffs;
	for (size_t coeff_index = 0; coeff_index < length; coeff_index++)
	{
		*coeff = static_cast<data_t>(*coeff * (0.42 - 0.5 *
			cos(pi_x2 * coeff_index / filter_order) + 0.08 *
			cos(pi_x2 * 2 * coeff_index / filter_order)));
		coeff++;
	}
}

template void make_linear_phase<>(size_t, float*);
template void make_linear_phase<>(size_t, double*);
template <typename data_t>
void make_linear_phase(size_t n_samples, data_t* out)
{
	assert(n_samples > 0);
	assert(!(n_samples % 2));
	
	/*
	auto phase_step = pi_x2 / n_samples;
	auto phase = phase_begin_value;
	for (size_t sample_index = 0; sample_index < n_samples; sample_index++)
	{
		*out++ = phase;
		phase -= static_cast<data_t>(phase_step);

		if (phase < -pi)
			phase += static_cast<data_t>(pi_x2);
	}
	*/
	for (size_t sample_index = 0; sample_index < n_samples / 2; sample_index++)
	{ 
		*out = static_cast<data_t>(-static_cast<ptrdiff_t>(n_samples - 1) * pi * sample_index / n_samples);
		out++;
	}
	for (size_t sample_index = n_samples / 2; sample_index < n_samples; sample_index++)
	{
		*out++ = static_cast<data_t>(-static_cast<ptrdiff_t>(n_samples - 1) * (static_cast<ptrdiff_t>(sample_index) - n_samples) * pi / n_samples);
	}
}
}
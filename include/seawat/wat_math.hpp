/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#ifndef WAT_MATH_HPP
#define WAT_MATH_HPP

#include <cstdint>
#include <complex>
#include <cmath>

namespace sea_wat
{
using namespace std;
using namespace std::complex_literals;
extern const double pi;
extern const double pi_x2;
extern const int64_t phase_scaler;
extern const int64_t pi_scaled;
extern const int64_t pi_x2_scaled;

double zeroeth_modified_Bessel_first_kind(double in);

template <typename data_t>
void sinc(data_t* out, size_t length, float f0, float f1, size_t out_datasize = sizeof(data_t));

template <typename data_t>
complex<float> dft(data_t* in, size_t n_samples, float band_rad,
    ptrdiff_t in_step, size_t start_phase_index = 0);
template <typename data_t>
complex<float> dft(complex<data_t>* in, size_t n_samples, float band_rad, size_t start_phase_index = 0);

template <typename data_t>
data_t inv_dft(complex<float>* in, size_t n_bands, size_t sample_index);

int64_t lcm(int64_t in0, int64_t in1);
int64_t gcd(int64_t in0, int64_t in1);

// dBFS & linear converters
template <typename out_data_t>
out_data_t dBFS_to_linear(float dBFS, size_t out_datasize = sizeof(out_data_t));
template <typename in_data_t>
float linear_to_dBFS(in_data_t linear, size_t in_datasize = sizeof(in_data_t));
// dB & linear converters
template <typename out_data_t>
out_data_t dB_to_linear(float dB);
template <typename in_data_t>
float linear_to_dB(in_data_t linear);

template <typename data_t>
void fit_to_Kaiser_win(data_t* coeffs, size_t length, float stop_band_dB);
template <typename data_t>
void fit_to_Hann_win(data_t* coeffs, size_t length);
template <typename data_t>
void fit_to_Bartlett_win(data_t* coeffs, size_t length);
template <typename data_t>
void fit_to_Blackman_win(data_t* coeffs, size_t length);

template <typename data_t>
void make_linear_phase(size_t n_samples, data_t* out);
}
#endif // WAT_MATH_HPP
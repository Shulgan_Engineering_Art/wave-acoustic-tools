/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#ifndef WAT_SINE_SYNTHESIZER_HPP
#define WAT_SINE_SYNTHESIZER_HPP

#include <cstdint>
#include <vector>
#include <functional>
#include "wat_primitives.hpp"

namespace sea_wat
{
using namespace std;

struct sine_synthesizer_set_Hz_t
{
    uint32_t samplerate_Hz;
    uint32_t freqency_Hz;
    size_t n_channels;
    float gain_dB;
    float start_phase = 0;
};

extern const sine_synthesizer_set_Hz_t sine_synthesizer_default;

template <typename data_t>
class sine_synthesizer_t
{
public:
    sine_synthesizer_t(const sine_synthesizer_set_Hz_t& set, 
        size_t out_datasize = sizeof(data_t));
    sine_synthesizer_t(size_t out_datasize = sizeof(data_t));
    virtual ~sine_synthesizer_t();
	size_t process(data_t* out, size_t n_samples, bool out_add = false);
	size_t process(data_t** out, size_t n_samples, bool out_add = false);
    size_t process(const vector<data_t*>& out,
		size_t n_samples, ptrdiff_t out_step, function<make_out_t<data_t>>& make_out);
    float get_phase();
    void set_gain(float gain);
    void set_gain_dB(float gain_dB);
    void set_freqency(uint32_t freqency_Hz);
protected:
    void init(const sine_synthesizer_set_Hz_t& set);
    void dealloc();

    data_t gain_scaler;
    float gain_dB;
    uint32_t samplerate_Hz;
    int64_t phase;
    int64_t phase_step;
    size_t n_channels;
    size_t out_datasize;
    int64_t switching_gain_time_samples;
};
}
#endif // WAT_SINE_SYNTHESIZER_HPP
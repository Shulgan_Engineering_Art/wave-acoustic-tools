/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#ifndef WAT_PRIMITIVES_HPP
#define WAT_PRIMITIVES_HPP

#include <cstdio>
#include <cstdint>
#include <functional>

namespace sea_wat
{
using namespace std;
template <typename data_t>
using make_out_t = void (data_t&, const data_t&);

// TODO: remove from header
template <typename data_t>
function<make_out_t<data_t>> make_out_move = [](data_t& out, const data_t& in){out = in;};
template <typename data_t>
function<make_out_t<data_t>> make_out_add = [](data_t& out, const data_t& in){out += in;};

extern const size_t sizeof_fixed24;

template <typename data_t>
data_t max_datatype_value(size_t in_datasize = sizeof(data_t));

// to 8 bit & format converters
template <typename in_data_t>
void to_fixed8(in_data_t* in,  int8_t* out,  size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed8(in_data_t** in, int8_t** out, size_t n_samples, size_t n_channels,
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed8(in_data_t** in, int8_t* out,  size_t n_samples, size_t n_channels,
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed8(in_data_t* in,  int8_t** out, size_t n_samples, size_t n_channels,
	size_t in_datasize = sizeof(in_data_t));

// to 16 bit & format converters
template <typename in_data_t>
void to_fixed16(in_data_t* in,  int16_t* out,  size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed16(in_data_t** in, int16_t** out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed16(in_data_t** in, int16_t* out,  size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed16(in_data_t* in,  int16_t** out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));

// to 24 bit & format converters
template <typename in_data_t>
void to_fixed24(in_data_t* in, int32_t* out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed24(in_data_t** in, int32_t** out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed24(in_data_t** in, int32_t* out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed24(in_data_t* in, int32_t** out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));

// to 32 bit & format converters
template <typename in_data_t>
void to_fixed32(in_data_t* in, int32_t* out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed32(in_data_t** in, int32_t** out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed32(in_data_t** in, int32_t* out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_fixed32(in_data_t* in, int32_t** out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));

// to float32 & format converters
template <typename in_data_t>
void to_float32(in_data_t* in,  float* out,  size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_float32(in_data_t** in, float** out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_float32(in_data_t** in, float* out,  size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_float32(in_data_t* in,  float** out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));

// to float64 & format converters
template <typename in_data_t>
void to_float64(in_data_t* in, double* out, size_t n_samples, size_t n_channels, 
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_float64(in_data_t** in, double** out, size_t n_samples, size_t n_channels,
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_float64(in_data_t** in, double* out, size_t n_samples, size_t n_channels,
	size_t in_datasize = sizeof(in_data_t));
template <typename in_data_t>
void to_float64(in_data_t* in, double** out, size_t n_samples, size_t n_channels,
	size_t in_datasize = sizeof(in_data_t));

// to mono converter
template <typename data_t>
void to_mono(data_t* in, data_t* out, size_t n_samples, size_t n_channels);
template <typename data_t>
void to_mono(data_t** in, data_t* out, size_t n_samples, size_t n_channels);

// reverser
template <typename data_t>
void data_reverse(data_t* in_out, size_t n_samples, size_t n_channels);
template <typename data_t>
void data_reverse(data_t* in, data_t* out, size_t n_samples, size_t n_channels);

}

#endif // WAT_PRIMITIVES_HPP

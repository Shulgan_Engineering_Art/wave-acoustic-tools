/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#ifndef WAT_SPECTRUM_ANALYZER_HPP
#define WAT_SPECTRUM_ANALYZER_HPP

#include <cstdint>
#include <complex>
#include <vector>

namespace sea_wat
{
using namespace std;

struct spectrum_analyzer_set_t
{
    size_t n_channels;
    size_t n_bands;
    float band_begin;
    float band_end;
};

extern const spectrum_analyzer_set_t spectrum_an_default;

template <typename data_t>
class spectrum_analyzer_t
{
public:
    //template <typename ref_t>
    spectrum_analyzer_t(const spectrum_analyzer_set_t& set);
    spectrum_analyzer_t();
    virtual ~spectrum_analyzer_t();
    size_t process(data_t* in, size_t n_samples);
    size_t process(data_t** in, size_t n_samples);
    void get_spectrums(vector<float*>* magnitudes, vector<float*>* phases);
    void get_spectrums(vector<complex<float>*>* spectrums);
protected:
    void init(const spectrum_analyzer_set_t& set);
    size_t core_routine(vector<data_t*> &in, size_t n_samples, ptrdiff_t in_step);
    void dealloc();
    vector<complex<float>*> spectrums;
    float band_begin;
    float band_step;
    size_t n_bands;
    size_t sample_index;
};
}
#endif // WAT_SPECTRUM_ANALYZER_HPP
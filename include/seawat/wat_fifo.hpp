/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#ifndef WAT_FIFO_HPP
#define WAT_FIFO_HPP

#include <system_error>
#include <vector>

namespace sea_wat
{
using namespace std;

template <typename data_t>
class fifo_t
{
public:
	fifo_t(size_t n_samples);
	virtual ~fifo_t();
	virtual void clear();
	virtual void fill_value(data_t value);
	
	virtual size_t write_begin(data_t** in);
	virtual error_code write_done(size_t n_samples);
	virtual size_t read_begin(data_t** out);
	virtual error_code read_done(size_t n_samples);
	virtual error_code write(data_t* in, size_t n_samples, ptrdiff_t in_step = 1);
	virtual error_code read(data_t* out, size_t n_samples, ptrdiff_t out_step = 1);

	virtual data_t* get_storage();
	virtual size_t get_n_samples_stored();
	bool is_full();
protected:
    size_t n_samples_size;
    size_t n_samples_stored;
	
	data_t* storage;
	data_t* read_ptr;
    data_t* write_ptr;
	data_t* after_last_ptr;
};
}
#endif /* WAT_FIFO_HPP */

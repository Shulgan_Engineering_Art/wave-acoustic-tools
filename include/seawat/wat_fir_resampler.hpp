/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#ifndef WAT_FIR_RESAMPLER_HPP
#define WAT_FIR_RESAMPLER_HPP

#include "wat_fir.hpp"

namespace sea_wat
{
using namespace std;

struct fir_resampler_set_Hz_t
{
	uint32_t in_samplerate_Hz;
	uint32_t out_samplerate_Hz;
	size_t n_channels;
	float stop_band_gain_dB;
};

extern const fir_resampler_set_Hz_t fir_resampler_default;

template <typename data_t>
class fir_resampler_t : public fir_t<data_t>
{
public:
    fir_resampler_t(const fir_resampler_set_Hz_t& set);
    fir_resampler_t();
    virtual ~fir_resampler_t();
	size_t process(data_t* in, data_t* out, size_t n_samples, bool out_add = false);
	size_t process(data_t** in, data_t** out, size_t n_samples, bool out_add = false);
	template <typename coeff_t>
	size_t process(const vector<data_t*> &in, const vector<data_t*> &out, size_t n_samples, 
		ptrdiff_t in_step, ptrdiff_t out_step, function<make_out_t<data_t>>& make_out);
	size_t get_flush_size();
	size_t flush(data_t* out, size_t n_out_samples, bool out_add = false);
	size_t flush(data_t** out, size_t n_out_samples, bool out_add = false);

	int32_t dec_scaler;
	int32_t int_scaler;
protected:
	void init(const fir_resampler_set_Hz_t& set);
	inline size_t required_in_for_n_out_samples(size_t n_out_samples);

	size_t n_in_sampes_per_length;
	size_t filter_tail;
    size_t n_prev_virtual_samples;
};
}

#endif // WAT_FIR_RESAMPLER_HPP

/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#ifndef WAT_WAVE_HPP
#define WAT_WAVE_HPP

#include <cstring>
#include <ios>
#include <vector>
#include <cstdint>
#include <system_error>
#include <fstream>
#include <filesystem>
#include <variant>
#include <optional>

namespace sea_wat 
{
using namespace std;

enum wave_encoding_tag
{
	WAVE_FORMAT_UNKNOWN = 0x0000,
	WAVE_FORMAT_PCM = 0x0001,
	WAVE_FORMAT_IEEE_FLOAT = 0x0003,
};

struct subchunk_header_t
{
	char chunk_id[4];
	uint32_t chunk_size;
};
struct chunk_RIFF_header_t
{
	subchunk_header_t head;
	char format[4];
};
struct subchunk_fmt_header_t
{
	subchunk_header_t head;
	uint16_t format;
	uint16_t n_channels;
	uint32_t sample_rate;
	uint32_t byte_rate;
	uint16_t block_align;
	uint16_t bits_per_sample;
};
struct subchunk_data_header_t
{
	subchunk_header_t head;
};

struct wave_read_set_t
{
	size_t n_samples_buffer_size;
	const filesystem::path& path;
	streamoff in_file_offset = 0;
};

struct wave_write_set_t
{
	wave_encoding_tag format;
	uint32_t samplerate;
	uint16_t n_channels;
	uint16_t bits_per_sample;
	size_t n_samples_buffer_size;
	const filesystem::path& path;
};

extern const chunk_RIFF_header_t default_RIFF;
extern const subchunk_fmt_header_t default_fmt;
extern const subchunk_data_header_t default_data;

using buffer_t = variant<int8_t*, int16_t*, int32_t*, float*, double*>;

class wave_t
{
public:
	wave_t();
	virtual ~wave_t();
	virtual size_t get_n_samples();
	virtual uint16_t get_n_channels() const;
	virtual uint32_t get_samplerate() const;
	virtual uint16_t get_bits_per_sample() const;
	virtual size_t get_samples_remaining();
	virtual size_t get_sample_index();
	error_code open_stream_last_error;
protected:
	error_code allocate_transition_buffer();
	void dealloc();
#if defined(WAT_IS_BIG_ENDIAN_SYS)
	inline void fmt_byteswap(subchunk_fmt_header_t *header);
#endif
	inline error_code state_check(size_t n_samples, size_t *file_bytes_per_sample);
	optional<buffer_t> transit_buffer;
	size_t current_sample_index;
	size_t n_samples_buffer_size;
	size_t n_samples_remaining;
	fstream stream;

	subchunk_fmt_header_t fmt;
	subchunk_data_header_t data;
};

class wave_reader_t : public wave_t
{
public:
	template <typename ref_t>
	error_code open(ref_t&& set);
	template <typename out_data_t>
	error_code read(out_data_t* out, size_t n_samples, size_t out_datasize = sizeof(out_data_t));
	template <typename out_data_t>
	error_code read(out_data_t** out, size_t n_samples, size_t out_datasize = sizeof(out_data_t));
	wave_encoding_tag get_format();
	error_code set_sample_index(size_t sample_index);
 protected:
	template <typename out_data_t>
	void read_samples(out_data_t* out, size_t n_samples, uint16_t n_channels, size_t out_datasize);
	error_code read_header();
};

class wave_writer_t : public wave_t
{
public:
	~wave_writer_t();
	template <typename ref_t>
	error_code open(ref_t&& set);
	template <typename in_data_t>
	error_code write(in_data_t* in, size_t n_samples, size_t in_datasize = sizeof(in_data_t));
	template <typename in_data_t>
	error_code write(in_data_t** in, size_t n_samples, size_t in_datasize = sizeof(in_data_t));
protected:
	error_code write_header(streamsize data_size);
	template <typename in_data_t>
	void write_samples(in_data_t* in, size_t n_samples, uint16_t n_channels, size_t in_datasize);
};
}  // namespace sea_wat

#endif  // WAT_WAVE_HPP

﻿/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#ifndef WAT_SPECTRUM_DFT_HPP
#define WAT_SPECTRUM_DFT_HPP

#include <cstdint>
#include <complex>
#include <vector>
#include <system_error>

namespace sea_wat
{
using namespace std;

struct spectrum_dft_set_t
{
    size_t n_channels;
    size_t n_bands;
    float band_begin;
    float band_end;
    size_t overlap_n_samples;
    size_t window_n_samples;
    size_t history_depth_n_windows;
};

extern const spectrum_dft_set_t spectrum_dft_default;

template <typename data_t>
class spectrum_dft_t
{
public:
    spectrum_dft_t(const spectrum_dft_set_t& set);
    spectrum_dft_t();
    virtual ~spectrum_dft_t();
    size_t process(data_t* in, size_t n_samples);
    size_t process(data_t** in, size_t n_samples);
    size_t process(const vector<data_t*> &in, size_t n_samples, ptrdiff_t in_step);
    error_code get_amplitudes(vector<vector<float*>>* amplitudes);
    error_code get_phases(vector<vector<float*>>* phases);
    vector<vector<complex<float>*>>* get_spectrums();
    void clear_history();
protected:
    void init(const spectrum_dft_set_t& set);
    void dealloc();
    inline void n_channels_dft(
        const vector<data_t*>& in, size_t n_samples, ptrdiff_t in_step);
    vector<vector<complex<float>*>> spectrums_history;  // window -> channel -> spectrum
    vector<vector<complex<float>*>> spectrum_parts;     // channel -> spectrum -> spectrum parts
    vector<size_t> window_parts_n_samples;
    float band_begin_rad;
    float band_step_rad;
    size_t n_channels;
    size_t n_bands;
    size_t sample_phase_index; // move to dft
    size_t samples_cnt;
    size_t window_part_index;

};
}
#endif // WAT_SPECTRUM_DFT_HPP
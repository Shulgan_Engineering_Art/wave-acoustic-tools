#ifndef IMPULSE_RESPONSE_PLOT_HPP
#define IMPULSE_RESPONSE_PLOT_HPP

#include <complex>

namespace sea_wat
{
using namespace std;

template <typename data_t>
void py_script_build(data_t* coeff, size_t length, string filename);

template <typename data_t>
void py_script_build(complex<data_t>* coeff, size_t length, string filename);
}

#endif
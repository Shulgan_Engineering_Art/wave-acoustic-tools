/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools.
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#ifndef WAT_FIR_WIENER_HOPF_HPP
#define WAT_FIR_WIENER_HOPF_HPP

#include <functional>
#include <vector>
#include "wat_fir.hpp"
#include "wat_fifo.hpp"
namespace sea_wat
{
using namespace std;

struct fir_Wiener_Hopf_set_t : fir_set_t
{
};

struct fir_Wiener_Hopf_set_Hz_t : fir_set_Hz_t
{
};


extern const fir_Wiener_Hopf_set_t fir_Wiener_Hopf_default;

template <typename data_t>
class fir_Wiener_Hopf_t : public fir_t<data_t>
{
public:
	fir_Wiener_Hopf_t(const fir_Wiener_Hopf_set_t& set);
	fir_Wiener_Hopf_t(const fir_Wiener_Hopf_set_Hz_t& set);
	fir_Wiener_Hopf_t();
	virtual ~fir_Wiener_Hopf_t();
	size_t process(data_t* in, data_t* in_ref, data_t* out_err, size_t n_samples, bool out_add = false);
	size_t process(data_t** in, data_t** in_ref, data_t** out_err, size_t n_samples, bool out_add = false);
	template <typename coeff_t>
	size_t process(const vector<data_t*>& in, const vector<data_t*>& in_ref, const vector<data_t*>& out_err,
		size_t n_samples, ptrdiff_t in_step, ptrdiff_t in_ref_step, ptrdiff_t out_step,
		function<void(data_t&, data_t&)>& make_out);
	void* get_model(size_t channel_index);
protected:
	void init(const fir_Wiener_Hopf_set_t& set);
	void dealloc();
	size_t n_channels;
	vector<void*> models;
};
}
#endif // WAT_FIR_WIENER_HOPF_HPP
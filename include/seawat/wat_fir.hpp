/***************************************************************************************************
wave-acoustic-tools Copyright (c) 2019 Shulgan Sergiy (Shulgan Engineering Art)
<https://gitlab.com/Shulgan_Engineering_Art/wave-acoustic-tools>
_______________________       ___       ________________
__  ___/__  ____/__    |      __ |     / /__    |__  __/  License: GNU GPL v3 or later.
_____ \__  __/  __  /| |________ | /| / /__  /| |_  /     SPDX-License-Identifier: GPL-3.0-or-later.
____/ /_  /___  _  ___ |/_____/_ |/ |/ / _  ___ |  /      Also available under commercial license.
/____/ /_____/  /_/  |_|      ____/|__/  /_/  |_/_/

wave-acoustic-tools is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either version 3 of
the License, or	(at your option) any later version.

wave-acoustic-tools is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with wave-acoustic-tools. 
If not, see <https://www.gnu.org/licenses/>.
***************************************************************************************************/

#ifndef WAT_FIR_HPP
#define WAT_FIR_HPP

#include <vector>
#include <functional>
#include "wat_primitives.hpp"

namespace sea_wat
{
using namespace std;

enum fir_type_t
{
	band_pass,
	band_stop
};

enum fir_response_t
{
	rectangular,
	Kaiser_win,
	Hann_win,
	Bartlett_win,
	Blackman_win
};

struct fir_set_t
{
	fir_type_t fir_type;
	fir_response_t fir_response_type;
	size_t n_channels;
	float band_begin;
	float band_end;
	float transition_width;
	float stop_band_gain_dB;
	float pass_band_gain_dB;
	float pass_band_ripple_dB;
};

struct fir_set_Hz_t
{
	fir_type_t fir_type;
	fir_response_t fir_response_type;
	size_t n_channels;
	uint32_t band_begin_Hz;
	uint32_t band_end_Hz;
	uint32_t transition_width_Hz;
	float stop_band_gain_dB;
	float pass_band_gain_dB;
	float pass_band_ripple_dB;
	uint32_t samplerate_Hz;
};

struct fir_set_fixed_len_t
{
	fir_type_t fir_type;
	fir_response_t fir_response_type;
	size_t n_channels;
	float band_begin;
	float band_end;
	size_t fir_length;
	float stop_band_gain_dB;
	float pass_band_gain_dB;
};

struct fir_set_fixed_len_Hz_t
{
	fir_type_t fir_type;
	fir_response_t fir_response_type;
	size_t n_channels;
	uint32_t band_begin_Hz;
	uint32_t band_end_Hz;
	size_t fir_length;
	float stop_band_gain_dB;
	float pass_band_gain_dB;
	uint32_t samplerate_Hz;
};

struct fir_set_external_t
{
	size_t n_channels;
	size_t fir_length;
};

extern const fir_set_t fir_default;

template <typename data_t>
class fir_t
{
public:
	fir_t(const fir_set_t& set);
	fir_t(const fir_set_Hz_t& set);
	fir_t(const fir_set_fixed_len_t& set);
	fir_t(const fir_set_fixed_len_Hz_t& set);
	fir_t(const fir_set_external_t& set);
    fir_t();
    virtual ~fir_t();
	size_t process(data_t* in, data_t* out, size_t n_samples);
	size_t process(data_t** in, data_t** out, size_t n_samples);
	template <typename coeff_t>
	size_t process(const vector<data_t*>& in, const vector<data_t*>& out, size_t n_samples,
		ptrdiff_t in_step, ptrdiff_t out_step);
	template <typename coeff_t>
    void gain_adj(coeff_t* coeffs, float band, float gain_dB);
	template <typename coeff_t>
    void set_external_coeffs(coeff_t* coeffs);
	void* get_model();
	size_t get_length();
protected:
	template <typename set_t>
	void init(const set_t& set);
	template <typename coeff_t>
	void make_response(float band_begin, float band_end, float transition_width,
		float stop_band_gain_dB, float pass_band_gain_dB, float pass_band_ripple_dB,
		fir_type_t fir_type, fir_response_t response_type);
	virtual void dealloc();

	void* filter_model;
	vector<void*> prev_samples;
	size_t length;
	size_t n_channels;
	bool is_external_coeffs;

	size_t start_delay_size;
	size_t flush_delay_size;
	int64_t out_gain_shift;
};

size_t fir_filter_length(float width, float stop_band_gain_dB, 
	float pass_band_gain_dB, float pass_band_ripple_dB);
}
#endif // WAT_FIR_HPP
